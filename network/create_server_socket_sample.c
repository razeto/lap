/* 
 * create_server_socket.c
 * 
 * $Id: create_server_socket_sample.c,v 1.1 2002-03-13 22:57:35 razeto Exp $
 * 
 * Copyright (C) 2001 Alessandro Razeto
 * Author: Alessandro Razeto <eto@linux.it>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Sample file
 *  
 */
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <errno.h>



#include "socket.h"

#ifdef _USE_GLOBAL
#include "global.h"
#else
#include <stdio.h>
#define FAIL -1 
#define RNT_ERROR(fmt, args...) ({ \
  fprintf(stderr, "!!! %s in %s,%d >> ", __PRETTY_FUNCTION__, __FILE__, __LINE__); \
  fprintf(stderr, fmt "\n", ##args); \
  FAIL; })
#define SYS_ERROR(fmt, args...) ({ \
  fprintf(stderr, "!!! %s in %s,%d> ", __PRETTY_FUNCTION__, __FILE__, __LINE__); \
  fprintf(stderr, fmt, ##args); \
  fprintf(stderr, ": %s (%d)\n", strerror(errno), errno);  \
  FAIL; })
#endif

/* ASSUME only TCP or UDP */

int socket_server_create (int proto, short int *port, unsigned long int local_address, int reuse_addr) {
  int socket_id;
  struct sockaddr_in server;
#if defined(__linux__) && __GNUC__ < 2
  char on = 1; /* this need as bolean for setsockoption */
#else
  int on = 1; /* this need as bolean for setsockoption */
#endif

  if ((proto != SOCK_DGRAM && proto != SOCK_STREAM) || !port)
    return RNT_ERROR ("wrong parameters (%d, %p)", proto, port);
  if ((socket_id = socket (PF_INET, proto, 0)) < 0) 
    return SYS_ERROR ("socket (%d)", proto);
  if (!local_address) local_address = INADDR_ANY;
  
  if (reuse_addr && setsockopt (socket_id, SOL_SOCKET, SO_REUSEADDR, (void *)&on, sizeof(on)))
    return SYS_ERROR ("setsockopt (SO_REUSEADDR)");
  

  
  /* FILL server structure  and call bind */
  server.sin_addr.s_addr = local_address;
  
  
  *port = server.sin_port;
  
  if (proto == SOCK_DGRAM) return socket_id;
  
  /* IF TCP CALL listen with QUEUE_LEN */
  ... 
  return socket_id;
}

int socket_accept (int server_socket) {
  int connected_socket = accept (server_socket, NULL, NULL);
  if (connected_socket < 0) return SYS_ERROR ("accept (%d)", server_socket);
  return connected_socket;
}

