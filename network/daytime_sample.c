#include <unistd.h>
#include <sys/types.h>
#include <string.h>
#include <stdio.h>
#include <netinet/in.h>
#include <time.h>
#include <syslog.h>

#include "socket.h"


int main (int a, char **b) {
  int socket_server, socket_connected;
  short int port = htons(4444);
  int accept;
  time_t currtime;

  /* BECOME DAEMON (EXERCISE 2) */

  openlog ("daytimeLAP", 0, LOG_DAEMON);

  /* CREATE PASSIVE SOCKET */
  socket_server = socket_server_create  (...);
  while (1) {
    socket_connected = socket_accept (socket_server);
    accept = accept_connection (socket_connected, "daytime");
    if (accept) {
      time (&currtime);
      if (write (socket_connected, ctime (&currtime), 25) < 0) {
	syslog(LOG_ERR, "write on socket, exiting");
	exit (1);
      }
    }
    close (socket_connected);
  }
  
  return 1;
}
