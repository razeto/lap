/* 
 * create_client_socket.c
 * 
 * $Id: create_client_socket.c,v 1.2 2002-04-02 06:52:06 ciro Exp $
 * 
 * Copyright (C) 2001 Alessandro Razeto
 * Author: Alessandro Razeto <eto@linux.it>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Working exercise
 *  
 */

#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <errno.h>


#include "socket.h"

#ifdef _USE_GLOBAL
#include "global.h"
#else
#include <stdio.h>
#define FAIL -1 
#define RNT_ERROR(fmt, args...) ({ \
  fprintf(stderr, "!!! %s in %s,%d >> ", __PRETTY_FUNCTION__, __FILE__, __LINE__); \
  fprintf(stderr, fmt "\n", ##args); \
  FAIL; })
#define SYS_ERROR(fmt, args...) ({ \
  fprintf(stderr, "!!! %s in %s,%d> ", __PRETTY_FUNCTION__, __FILE__, __LINE__); \
  fprintf(stderr, fmt, ##args); \
  fprintf(stderr, ": %s (%d)\n", strerror(errno), errno);  \
  FAIL; })
#endif

/* ASSUME only TCP or UDP */

int socket_client_create (int proto, short int remote_port, unsigned long int remote_address, 
    short int local_port, unsigned long int local_address) {
  int socket_id;
  struct sockaddr_in remote, local;


  if (proto != SOCK_DGRAM && proto != SOCK_STREAM)
    return RNT_ERROR ("wrong protocol (%d)", proto);

  if (!remote_port) return RNT_ERROR ("wrong port (%d)", remote_port);
    
  
  if ((socket_id = socket (PF_INET, proto, 0)) < 0) 
    return SYS_ERROR ("socket (%d)", proto);


  if (local_address || local_port) {
    memset(&local, 0, sizeof(local));
    local.sin_addr.s_addr = local_address;
    local.sin_port = local_port;
    local.sin_family = AF_INET;
    if (bind (socket_id, (struct sockaddr *)&local, sizeof(local)) < 0)
      return SYS_ERROR ("bind");
  }
  
  memset(&remote, 0, sizeof(remote));
  remote.sin_addr.s_addr = remote_address;
  remote.sin_port = remote_port;
  remote.sin_family = AF_INET;
  if (connect (socket_id, (struct sockaddr *)&remote, sizeof(remote)) < 0)
    return SYS_ERROR ("connect");

  return socket_id;
}

