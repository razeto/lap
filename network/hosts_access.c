/* 
 * hosts_access.c: allow or deny connection using tcpd library
 * 
 * $Id: hosts_access.c,v 1.3 2002-04-21 17:07:02 razeto Exp $
 * 
 * Copyright (C) 2001 Alessandro Razeto
 * Author: Alessandro Razeto <eto@linux.it>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Working exercise
 */
#include <stdlib.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <netdb.h>
#include <errno.h>
#include <tcpd.h>
#include <ident.h>

#include "socket.h"


#if HAVE_REDHAT
int allow_severity;
int deny_severity;
#endif

#ifdef _USE_GLOBAL
#include "global.h"
#else
#include <stdio.h>
#define FAIL -1 
#define SYS_ERROR(fmt, args...) ({ \
  fprintf(stderr, "!!! %s in %s,%d >> ", __PRETTY_FUNCTION__, __FILE__, __LINE__); \
  fprintf(stderr, fmt, ##args); \
  fprintf(stderr, ": %s (%d)\n", strerror(errno), errno);  \
  FAIL; })
#define MSG(fmt, args...)       fprintf(stderr, fmt, ##args)
#endif





int accept_connection (int socket, const char *name) {
  struct sockaddr_in server_global, client_global;
  char client_ip[20], client_name[100];
  char server_ip[20], server_name[100];
  char client_username[100];
  struct hostent *client_hostent, *server_hostent;
  socklen_t len;
  void * ptr;
  struct request_info request;
  int ok;
  
    /* Initialize with fd and name of the program */ 
  request_init (&request, RQ_FILE, socket, RQ_DAEMON, name, 0);

    /* GET the peer SIN data and fill in the request */
  len = sizeof (struct sockaddr_in);
  if (getpeername (socket, (struct sockaddr *)&client_global, &len) != 0) 
    SYS_ERROR ("getpeername on %d", socket);
  request_set (&request, RQ_CLIENT_SIN, &client_global,0);
    /* GET ASCII coding of client IP  and fill in the request */ 
  ptr = inet_ntoa (client_global.sin_addr);
  strncpy (client_ip, ptr, sizeof (client_ip));
  request_set (&request, RQ_CLIENT_ADDR, client_ip, 0);
   /* GET client name and fill in the request */
  client_hostent = gethostbyaddr ((char *)&(client_global.sin_addr.s_addr), 
      sizeof (client_global.sin_addr.s_addr), AF_INET);
  if (!client_hostent) SYS_ERROR ("gethostbyaddr (%s)", client_ip);
  else strncpy (client_name, client_hostent->h_name, sizeof (client_name));
  request_set (&request, RQ_CLIENT_NAME, client_name);
      
    /* GET the local SIN data and fill in the request */
  len = sizeof (struct sockaddr_in);
  if (getsockname (socket, (struct sockaddr *)&server_global, &len) != 0) {
    return SYS_ERROR ("getsockname on %d", socket);
  }
  request_set (&request, RQ_SERVER_SIN, &server_global, 0);
    /* GET ASCII coding of server IP  and fill in the request */
  ptr = inet_ntoa (server_global.sin_addr);
  strncpy (server_ip, (char *)ptr, sizeof (server_ip));
  request_set (&request, RQ_SERVER_ADDR, server_ip, 0);
   /* GET server name and fill in the request */
  server_hostent = gethostbyaddr ((char *)&(server_global.sin_addr.s_addr), 
      sizeof (server_global.sin_addr.s_addr), AF_INET);
  if (!server_hostent) SYS_ERROR ("gethostbyaddr (%s)", server_ip);
  else strncpy (server_name, server_hostent->h_name, sizeof (server_name));
  request_set (&request, RQ_SERVER_NAME, server_name);
  
   /* GET the client user name and fill in the request */
  ptr = ident_id(socket, IDENT_TIMEOUT);
  if (ptr) {
    strncpy (client_username, (char *)ptr, sizeof (client_username));
    request_set (&request, RQ_USER, client_username, 0);
//    MSG ("Connection from %s@%s: ", ptr, client_name);
    free (ptr);
  }
    
  ok = hosts_access (&request);
  if (ok < 0) return SYS_ERROR ("request_init");
  //MSG ("%s\n", ok?"accepted":"refused");
  return ok?ACCEPT:DENY;
}
