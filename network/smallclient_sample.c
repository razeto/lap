/* 
 * smallclient.c: a small daytime client
 * 
 * $Id: smallclient_sample.c,v 1.2 2002-04-02 06:55:48 ciro Exp $
 * 
 * Copyright (C) 2001 Alessandro Razeto
 * Author: Alessandro Razeto <eto@linux.it>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Sample file
 *  
 */
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <netdb.h>

int main (int argc, char *argv[]) {
  struct sockaddr_in remote;
  unsigned long remote_address;
  int socket_id;
  int ret;
  char buff[255];
  
  /* CALL SOCKET */
  ...

  
  if (argc != 2) {
    printf ("wrong arguments\n");
    exit (1);
  }

  /* CONVERT ARGV[1] to an address */


  /* fill the remote structure */

  /* call connect */

  while (1) {
   ret = read (socket_id, buff, sizeof (buff));
   if (ret < 0) {
    perror ("read");
    exit (1);
   }
   if (!ret) break;
   write (1, buff, ret);
  }
  
  close (socket_id);

  exit (0);
}

