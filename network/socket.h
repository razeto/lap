#ifndef _CREATE_SERVER_SOCKET_H
#define _CREATE_SERVER_SOCKET_H

#include <sys/socket.h>

#define TCP SOCK_STREAM
#define UDP SOCK_DGRAM
#define QUEUE_LEN 10
int socket_server_create (int proto, short int *port, unsigned long int local_address, int reuse_addr);
int socket_accept (int server_socket);
int socket_client_create (int proto, short int remote_port, unsigned long int remote_address, 
        short int local_port, unsigned long int local_address);

#define ACCEPT  137
#define DENY    0
#define IDENT_TIMEOUT 20


int accept_connection (int socket, const char *name);



#endif
