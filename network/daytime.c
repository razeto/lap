#include <unistd.h>
#include <sys/types.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <time.h>
#include <syslog.h>

#include "socket.h"


int main (int a, char **b) {
  int socket_server, socket_connected;
  short int port = htons(4444);
  int accept;
  time_t currtime;
  pid_t pid;
  
  pid = fork ();
  if (pid < 0) { 
    perror ("fork");
    exit (1);
  }
  if (pid) exit (0);

  setsid ();
  chdir("/");
  openlog ("daytimeLAP", 0, LOG_DAEMON);

  socket_server = socket_server_create (TCP, &port, 0, 1);
  if (socket_server < 0)
    exit(1);

  close (0);
  close (1);
  close (2);

  while (1) {
    socket_connected = socket_accept (socket_server);
    accept = accept_connection (socket_connected, "daytime");
    if (accept) {
      time (&currtime);
      if (write (socket_connected, ctime (&currtime), 25) < 0) {
	syslog(LOG_ERR, "write on socket, exiting");
	exit (1);
      }
    }
    close (socket_connected);
  }
  
  return 1;
}
