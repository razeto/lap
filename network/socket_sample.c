/* 
 * socket.c: create a connection and communicate by socket
 * 
 * $Id: socket_sample.c,v 1.2 2002-03-13 22:57:36 razeto Exp $
 * 
 * Copyright (C) 2001 Alessandro Razeto
 * Author: Alessandro Razeto <eto@linux.it>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Working exercise
 */
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <libgen.h>
#define _GNU_SOURCE
#include <getopt.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/poll.h>


#include "socket.h"

void usage (char *argv[]) {
  fprintf (stderr, "%s [OPTIONS] machine port/service\n", basename (argv[0]));
  fprintf (stderr, "    -u, --udp\t\t\t use UDP transport protocol\n");
  fprintf (stderr, "    -t, --tcp\t\t\t use TCP transport protocol\n");
  fprintf (stderr, "    -P, --localport port\t set the outgoing port\n");
  fprintf (stderr, "    -A, --localaddress address\t set the outgoing adress\n");
  fprintf (stderr, "    -h, --help\t\t\t this help\n");
}

struct in_addr get_address (char *address_str) {
  struct in_addr address;
  struct hostent *name;
  /* test if adress is a dotted point one (use inet_aton) */
  /* if not search through the DNS database for its ip using gethostbyname */
  { ...
    address = *(struct in_addr *)(name->h_addr_list[0]);
  }
  return address; 
}
unsigned short get_port (char *port_str) {
  unsigned short port = htons (atoi (port_str));
  /* if port_str is not a number search in the services database using
   * getservbyname */

  return port;
}

/* WRITE count bytes!!! A bogus technique is used, pay attention: send and
 * write have diffent number of arguments; there may be some kind of stack 
 * corruption. However adding arguments is normal safe */
void writen (int fd, char *buffer, size_t count, ssize_t (*method)(int, const void *, size_t, int)) {
  int ret;
  while (count > 0) {
    ret = method (fd, buffer, count, 0);
    if (ret <0) {
      perror ("send");
      exit (1);
    }
    buffer += ret;
    count -= ret;
  }
}
  

int main (int argc, char *argv[]) {
  int fd;
  char buff[1000];
  int read_count;
  struct pollfd fds[2];


  int proto = -1;
  char *local_port_str = NULL;
  unsigned short local_port = 0;
  char *local_address_str = NULL;
  struct in_addr local_address = { 0UL };
  char *remote_address_str = NULL;
  struct in_addr remote_address;
  char *remote_port_str = NULL;
  unsigned short remote_port = 0;

  /* PROCESS the arguments: refer to the previus description (usage for the
   * arguments. 
   * USE getopt_long!!!
   */

  do  {
    /* a do {} while (0) allow to declare a standalone block in which automaitc
     * variables can be declared */
    int c;
    extern char *optarg;
    extern int optind;
    /* Declare and fill the structures needed by getoptlong */
    char short_options[]=...
    static struct option long_options[] = ...
      ...
      {0, 0, NULL, 0}
    };
    while (1) {
      c = getopt_long (argc, argv, short_options, long_options, NULL);
      if (c == -1) break;
      switch (c) {
	case 'u':
	  if (proto != -1) {
	    fprintf (stderr, "Protocol already specified; ignoring -u\n");
	    break;
	  }
	  proto = UDP;
	  break;
	case 't':
	  if (proto != -1) {
	    fprintf (stderr, "Protocol already specified; ignoring -t\n");
	    break;
	  }
	  proto = TCP;
	  break;
	case 'P':
	  local_port_str = optarg;
	  local_port = get_port (local_port_str);
	  break;
	case 'A':
	  local_address_str = optarg;
	  local_address = get_address (local_address_str);
	  break;
	case 'h':
	  usage (argv);
	  break;
	default:
	  fprintf (stderr, "Unknown option\n");
	  usage (argv);
	  exit (1);
      }
    }
    if (optind + 2 < argc) {
      fprintf (stderr, "Too few arguments\n");
      usage (argv);
      exit (1);
    }
    if (optind + 2 > argc) {
      fprintf (stderr, "Too many arguments\n");
      usage (argv);
      exit (1);
    }
    remote_port_str = argv[optind + 1];
    remote_port = get_port (remote_port_str);

    remote_address_str = argv[optind];
    remote_address = get_address (remote_address_str);
    
    if (proto == -1) proto = TCP;

    fprintf (stderr, "Trying %s connection %s:%d", (proto == TCP)?"TCP":"UDP", 
	inet_ntoa(local_address), htons (local_port));
    fprintf (stderr, "-> %s:%d\n", 
	inet_ntoa(remote_address), htons (remote_port));

  } while (0);

  /* FILL THE FUNCTION */
  fd = socket_client_create (...);
  if (fd < 0) exit (1);

  /* fill the fds[] structures */


  while (1) {
    /* call poll */
    
    if (fds[0].revents & (POLLIN | POLLHUP)) {
      read_count = read (0, buff, sizeof (buff));
      if (read_count < 0) {
	perror ("read");
	exit (1);
      }
      if (!read_count)  exit (0);
      writen (fd, buff, read_count, send);
    }
    if (fds[0].revents & (POLLIN | POLLHUP)) {
      read_count = recv(fd, buff, sizeof (buff), 0);
      if (read_count < 0) {
	perror ("read");
	exit (1);
      }
      if (!read_count) exit (0);
      writen (1, buff, read_count, write); 
    }
  }
  
  return 0;
}
