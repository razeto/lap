all: 
	echo "do nothing"

clean:
	$(MAKE) -C esh clean
	$(MAKE) -C examples clean
	$(MAKE) -C multicat clean
	$(MAKE) -C mwrite clean
	$(MAKE) -C mycat clean
	$(MAKE) -C ipc clean
	$(MAKE) -C network clean
	$(MAKE) -C dlopen clean
