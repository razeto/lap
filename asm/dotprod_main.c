/*
 * dotprod_main.c
 * Driver program for dot product routines implemented
 * in dotprot_nosimd.c and dotprod_simd.c .
 * by Ciro Cattuto
 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

/* a simple macro to verify 16-boundary alignment of a pointer */
#define UNALIGNED16(ptr) ((unsigned long) (ptr) & 0xf)

#define	LEN 100000L /* vector size */

extern float dot_product(float *x1, float *x2, unsigned long len);

int main(void)
{
	unsigned long i, count;
	volatile float dot;

#ifdef ALIGNED
	/* page-aligned memory allocation (we need 16-byte alignment) */
	float *x1 = (float *) valloc(LEN * sizeof(float));
	float *x2 = (float *) valloc(LEN * sizeof(float));
#else
	/* unaligned memory allocation */
	float *x1 = (float *) malloc(LEN * sizeof(float));
	float *x2 = (float *) malloc(LEN * sizeof(float));
#endif

	/* verify successful allocation and proper alignment */
	assert(x1 != NULL && x2 != NULL);
#ifdef ALIGNED
	assert(!UNALIGNED16(x1) && !UNALIGNED16(x2));
#endif

	/* initialize the vectors */
	for (i=0; i<LEN; i++) {
		x1[i] = 0.2 * (i % 71);
		x2[i] = 0.3 * (i % 42);
	}

	/* perform scalar product (5000 times) */
	/* ("dot" was declared volatile so that the loop
	 * doesn't get optimized away) */
	for (count=0; count<5000; count++)
		dot = dot_product(x1, x2, LEN);

	printf("Computed dot product as %e\n", dot);

	return 0;
}

