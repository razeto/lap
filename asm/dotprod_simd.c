/*
 * Performs the dot product of vectors x1 and x2 using SIMD instructions
 * (x1 and x2 are assumed to be aligned on a 16-byte boundary).
 * by Ciro Cattuto
 */

float dot_product(float *x1, float *x2, unsigned long len)
{
	float *end1 = x1 + len;
	float xmm_sum[4];

	/* zero out xmm0 register */
	asm __volatile__ ( "xorps %xmm0, %xmm0" );

	/* main loop */
	while (x1 < end1) {
		asm __volatile__ (
			"movaps	(%0), %%xmm1	\n\t"
			"addl	$16, %0			\n\t"
			"mulps	(%1), %%xmm1	\n\t"
			"addl	$16, %1			\n\t"
			"addps	%%xmm1, %%xmm0	\n\t"
			: "=r" (x1), "=r" (x2)
			: "0" (x1), "1" (x2)
		);
	}

	/* move xmm0 to xmm_sum[] array */
	asm __volatile__ ( "movups %%xmm0, %0" : "=m" (xmm_sum) );

	/* return global sum */
	return (xmm_sum[0] + xmm_sum[1] + xmm_sum[2] + xmm_sum[3]);
}

