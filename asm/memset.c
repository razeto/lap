/*
 * memset.c
 * Shows a basic assembly implementation of memset(3) for chars
 * (assumes that sizeof(char) == 1).
 * by Ciro Cattuto
 */

#include <stdio.h>
#include <string.h>
#include <assert.h>

#define LEN		30		/* array length */

/* (LEN+1)-array of long words, initialized to zero */
char x[LEN+1] = {0, };


/* simple implementation of memset(3), using inline x86 assembly */
inline void my_memset(char *ptr, char value, int len)
{
	char *dummy_ptr;
	int dummy_len;

	asm __volatile__ (
		"cld	\n\t"
		"rep	\n\t"
		"stosb	\n\t"
		: "=c" (dummy_len), "=D" (dummy_ptr)
		: "0" (len), "a" (value), "1" (ptr)
	);
}


int main(void)
{
	assert(sizeof(char) == 1); /* just to be safe... */

	/* set "x" array using the standard memset(3) library function */
	memset(x, 'A', LEN);
	puts(x);

	/* set "x" array using our assembly memset implementation */
	my_memset(x, 'B', LEN);
	puts(x);

	return 0;
}

