/*
 * rdtsc.c
 * Reads TSC (Time Stamp Counter) register.
 * by Ciro Cattuto
 */

#include <stdio.h>

/* 
 * A macro that inlines a "rdtsc" intruction.
 *
 *  "=A" is the output value (64-bit unsigned long, composed of EAX and EDX).
 *  There are no inputs.
 *
 */
#define RDTSC(llptr)	do {							\
	__asm__ __volatile__ (".byte 0x0f ; .byte 0x31"		\
	: "=A" (llptr) : /* no inputs */  );				\
	} while(0)

int main(int argc, char *argv[])
{
	unsigned long long tsc_value;

	/* read counter */
	RDTSC(tsc_value);

	/* print it out */
	printf("TSC = %lld\n", tsc_value);

	return 0;
}

