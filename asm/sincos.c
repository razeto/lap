/*
 * sincos.c
 * Illustrates how to call the "fsincos" x86 instruction from C code.
 * by Ciro Cattuto
 */

#include <stdio.h>

int main(int argc, char *argv[])
{
	double x = 1.0;
	double sinx, cosx;

	/* 
	 * Inline the "fsincos" x86 intruction: fsincos pops the value
	 * at the top of the FP stack, and pushes on the same stack
	 * the computed values of its sine and cosine.
	 *
	 *  "=t" is an output (sin x) operand, at the top of the FP stack.
	 *
	 *  "=u" is an output (cos x) operand, next-to-top on the FP stack.
	 *
	 *  "0"  is an input (x) operand, identified with the first output
	 *       operand ("=t"), located at the top of the FP stack.
	 */
	asm ( "fsincos" : "=t" (cosx), "=u" (sinx) : "0" (x) );

	/* display results */
	printf("x=%e : sin(x)=%e, cos(x)=%e\n", x, sinx, cosx);

	return 0;
}

