/*
 * Performs the dot product of vectors x1 and x2.
 * by Ciro Cattuto
 */

float dot_product(float *x1, float *x2, unsigned long len)
{
	float *end1 = x1 + len;
	float sum = 0.0;

	/* compute dot product */
	while (x1 < end1)
		sum += *(x1++) * *(x2++);

	return sum;
}

