/*
 * add.c
 * Performs an integer sum by inlining a x86 "add" instruction.
 * by Ciro Cattuto
 */

#include <stdio.h>

int main(int argc, char *argv[])
{
	long x = 444, y = -402;
	long sum;

	/* 
	 * Inline the "add" x86 intruction.
	 *
	 *  "=r" is the output (x+y) value (overwrites the %0 operand)
	 *  "r"  is an input (x) operand (loaded into %1)
	 *  "0"  is an input (y) operand (loaded into %0)
	 *
	 */
	asm ( "add %1, %0" : "=r" (sum) : "r" (x), "0" (y) );

	/* display results */
	printf("x=%ld, y=%ld : x+y=%ld\n", x, y, sum);

	return 0;
}

