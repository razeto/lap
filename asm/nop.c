/*
 * nop.c
 * Does nothing - in a complicated way
 * by Ciro Cattuto
 */

int main(int argc, char *argv[])
{
#if 0
	volatile int i = 0;
#endif

	/* inlines the x86 "nop" instruction */
	asm ( "nop" );

	return 0;
}

