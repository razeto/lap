/*
 * call.c
 * Performs a procedure call at the assembly level, passing parameters.
 * by Ciro Cattuto
 */

#include <stdio.h>

/* a trivial procedure, taking a single argument */
void myproc(int a, int b)
{
	printf("Here I am, with arguments a=%d and b=%d.\n", a, b);
}

int main(void)
{
	int x = 42, y = 0;
	void (*f)(int,int) = myproc;

	/* We call it the usual way... */
	myproc(x,y);

	/* ...another way... */
	(*f)(x,y);

	/* ...and finally we call it at the assembly level
	 * (notice the order we follow in pushing arguments onto the stack). */
	asm (
		"push %0	\n\t"
		"push %1	\n\t"
		"call *%2	\n\t"
		: /* no outputs */
		: "r" (y), "r" (x), "r" (&myproc)
	);

	return 0;
}

