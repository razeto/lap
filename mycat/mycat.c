/* 
 * mycut.c: copy input on output counting lines
 * 
 * $Id: mycat.c,v 1.1 2002-03-02 09:38:50 razeto Exp $
 * 
 * Copyright (C) 2001 Alessandro Razeto
 * Author: Alessandro Razeto <eto@linux.it>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Working exercise
 */


#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]) {
  int lines = 0;
  int buff [255];
  
  //setvbuf(stdout, 0, _IOLBF, 0);
  while (1) {
    char *ptr;
    int ret;     
    ptr = fgets ((void *)buff, sizeof (buff), stdin);
    
    if (!ptr) {
      if (feof (stdin)) break;
      fprintf (stderr, "error on stdin::");
      perror ("fgets");
      exit (1);
    }
    
    ret = fputs (ptr, stdout);
    if (ret == EOF) {
      fprintf (stderr, "error on stdout::");
      perror ("fputs");
      fprintf (stderr, "Printed %d lines\n", lines);
      exit (1);
    }
    lines++;
  }
  fprintf (stderr, "Printed %d lines\n", lines);
  return 0;
}
