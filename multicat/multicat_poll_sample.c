/* 
 * multicat_poll.c
 * 
 * $Id: multicat_poll_sample.c,v 1.1 2002-03-03 10:22:08 razeto Exp $
 * 
 * Copyright (C) 2001 Alessandro Razeto
 * Author: Alessandro Razeto <eto@linux.it>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * sample file
 */


#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <libgen.h>
#include <unistd.h>
#include <sys/poll.h>

int main (int argc, char *argv[]) {
  char buff [255];
  
  struct pollfd fd[10];
  int fds = 0;
  int i;
  
  memset (fd, 0, sizeof (fd));

  for (i = 0; i < 10; i++) {
    if (!argv[i + 1]) break;
    /* open the files and fill the fd[i] structures */
    fds ++;
  }
  if (i == 0) { 
    fprintf (stderr, "Usage %s filenames\n", basename (argv[0]));
    exit (1);
  }
    

  while (fds) {
    int read_count;
    int write_count;
    char * ptr = buff;
    /* call poll */
    int poll_ret = poll ...

    if (poll_ret < 0) {
      if (errno == EINTR) continue;
      perror ("poll");
      exit (1);
    }
    /* loop on ready file descriptor */
    /* hint: POLLHUP is a normal condition as POLLIN */


    /* if read returns zero close the file descriptoe */
    /* PAY attention fd[[] has to have only opened fd */

      if (read_count < 0) {
	fprintf (stderr, "read failed on %d (%s): %s\n", fd[i].fd, argv[i + 1], strerror (errno));
	exit (1);
      }
      ptr = buff;
      while (read_count) {
        write_count = write (1, ptr, read_count);
	if (write_count < 0) {
	  perror ("write on stdout");
	  exit (1);
	}
	read_count -= write_count;
	ptr += write_count;
      }
    }
  }
    
  return 0;
}
