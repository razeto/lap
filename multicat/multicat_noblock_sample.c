/* 
 * multicat_noblock.c
 * 
 * $Id: multicat_noblock_sample.c,v 1.4 2002-04-02 06:45:58 ciro Exp $
 * 
 * Copyright (C) 2001 Alessandro Razeto
 * Author: Alessandro Razeto <eto@linux.it>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Exercise
 */


#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <libgen.h>
#include <unistd.h>
#include <sys/stat.h>


int main (int argc, char *argv[]) {
  char buff [255];
  
  int fd[10] = {0, };
  int fds = 0;
  int i;
  struct stat file_info;
  int flags;
  
  for (i = 0; i < 10; i++) {
    if (argv[i + 1] == NULL) break;
    if (stat (argv[i + 1], &file_info) < 0) {
      fprintf (stderr, "fstat (%s) failed: %s\n", argv[i + 1], strerror (errno));
      exit (1);
    }
    /* OPEN THE FILES */
    
    if (fd[i] < 0) {
      fprintf (stderr, "open (%s) failed: %s\n", argv[i + 1], strerror (errno));
      exit (1);
    }
    fds ++;
  }
  if (i == 0) { 
    fprintf (stderr, "Usage %s filenames\n", basename (argv[0]));
    exit (1);
  }
    

  while (fds) {
    int read_count;
    int write_count;
    char * ptr = buff;
    for (i = 0; i < 10; i++) {
      if (!fd[i]) continue; 
      /* READ the ith file  in buff */
      
      /* CHECK EOF and cose the fd */
      
      /* CHECK ERRORS */

      ptr = buff;
      while (read_count) {
        write_count = write (1, ptr, read_count);
	if (write_count < 0) {
	  perror ("write on stdout");
	  exit (1);
	}
	read_count -= write_count;
	ptr += write_count;
      }
    }
  }
    
  return 0;
}
