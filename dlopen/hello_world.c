#include <stdio.h>
#include <stdlib.h>
#include <dlfcn.h>


int main (int argc, char *argv[]) {
  void *module;
  int (*greeting)(void * data); 
  void (*init)(void);
  int good;
  char *message = "ciao a tutti";
  
  module = dlopen ("libmodule.so", RTLD_NOW);
  if (!module) {
    perror ("dlopen(libmodule.so)");
    fprintf (stderr, ">> %s\n", dlerror ());
    exit (1);
  }

  greeting = dlsym(module, "say_hello");
  if (!greeting) {
    perror ("dlsym(module, \"say_hello\")");
    fprintf (stderr, ">> %s\n", dlerror ());
    exit (1);
  }

  good = (*greeting)(message);
  if (good < 0) {
    fprintf (stderr, "function greetings returned %d\n", good);
  }

  /* do something more then exercize */
  init = dlsym(module, "_init");
  if (!init) {
    perror ("dlsym(module, \"say_hello\")");
    fprintf (stderr, ">> %s\n", dlerror ());
    exit (1);
  }
  (*init)();
  good = (*greeting)("bye");
  if (good < 0) {
    fprintf (stderr, "function greetings returned %d\n", good);
  }

  
  return 0;

}
