#include <stdio.h>

static char _module_id[50];
static int _count=0;
void _init (void) {
  _count++;
  snprintf (_module_id, sizeof(_module_id), "%s_v%d", "libmodule",_count);
  return;
}
  
int say_hello (void *data) {
  char *string = data;

  if (!string) return -1;

  printf ("%s: %s\n", _module_id , string);

  return 0;

}

