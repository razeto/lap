/* 
 * sem_sysv.c: some job on counting semaphore
 * 
 * $Id: sem_sysv_sample.c,v 1.1 2002-03-06 10:00:22 razeto Exp $
 * 
 * Copyright (C) 2001 Alessandro Razeto
 * Author: Alessandro Razeto <eto@linux.it>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  sample file
 */
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <fcntl.h>
#include <sys/stat.h>


int main (int argc, char **argv) {
  int count = 400;
  pid_t pid;
  int id;
  /* attach the semafores */
  /* hint use IPC_CREAT and S_IRWXU */

  pid = fork ();
  if (pid < 0) {
    perror ("fork");
    exit (1);
  } 
  
  if (!pid) {
    /* child */
    struct sembuf operation;
    while (1) {
      /* decrement sem 0 */

      /* sleep some small time */
      usleep (...);

      /* increment sem 1 */
    }
  } else {
    struct sembuf operation;
    while (count--) {
      /* increment sem 0 */
      
      /* decrement sem 1 */
      
      }
    }
  }
  kill (pid);
  return 0;
}
