/* 
 * sem_sysv.c: some job on counting semaphore
 * 
 * $Id: sem_sysv.c,v 1.3 2002-04-02 06:41:36 ciro Exp $
 * 
 * Copyright (C) 2001 Alessandro Razeto
 * Author: Alessandro Razeto <eto@linux.it>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Working exercise
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <signal.h>


int main (int argc, char **argv) {
  int count = 100;
  pid_t pid;
  int id = semget (1979, 2, IPC_CREAT | S_IRWXU);
  if (id < 0) {
    perror ("semget");
    exit (1);
  }

  pid = fork ();
  if (pid < 0) {
    perror ("fork");
    exit (1);
  } 
  
  if (!pid) {
    /* child */
    struct sembuf operation;
    while (1) {
      operation.sem_num = 0;
      operation.sem_op = -1;
      operation.sem_flg = 0;
      if (semop (id, &operation, 1) < 0) {
	    perror ("semop");
	    exit (1);
      }
	  fprintf(stdout, "child: decremented semaphore 0\n");
	  fflush(stdout);

      sleep(1);

      operation.sem_num = 1;
      operation.sem_op = 1;
      operation.sem_flg = 0;
      if (semop (id, &operation, 1) < 0) {
	    perror ("semop");
	    exit (1);
      }
	  fprintf(stdout, "child: incremented semaphore 1\n");
	  fflush(stdout);
    }
  } else {
    /* parent */
    struct sembuf operation;
    while (count--) {
      operation.sem_num = 0;
      operation.sem_op = 1;
      operation.sem_flg = 0;
      if (semop (id, &operation, 1) < 0) {
	    perror ("semop");
	    exit (1);
      }
	  fprintf(stdout, "parent: incremented semaphore 0\n");
	  fflush(stdout);

      operation.sem_num = 1;
      operation.sem_op = -1;
      operation.sem_flg = 0;
      if (semop (id, &operation, 1) < 0) {
	    perror ("semop");
	    exit (1);
      }
	  fprintf(stdout, "parent: decremented semaphore 1\n");
	  fflush(stdout);
    }
  }
  kill (pid, SIGINT);
  return 0;
}
