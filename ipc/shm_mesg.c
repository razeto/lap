/* 
 * shm_mesg.c: read or write a message on shared memory
 * 
 * $Id: shm_mesg.c,v 1.2 2002-04-02 06:42:38 ciro Exp $
 * 
 * Copyright (C) 2001 Alessandro Razeto
 * Author: Alessandro Razeto <eto@linux.it>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Working exercise
 */


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <time.h>


#define DEFKEY 		1974

struct info {
  int present;
  size_t size;
  time_t date;
  int priority;
} *info;

#define INFOSIZE	(sizeof(struct info))
#define MSGSIZE	    1000
#define SIZE		(INFOSIZE+MSGSIZE)

int main (int argc, char *argv[]) {
  int key = DEFKEY;
  int read = 1;
  int prio = 1;
  char *line;
  int fd;
  void *shm;
  
  do {
    int c;
    extern char *optarg;
    extern int optind;
    while (1) {
      c=getopt (argc,argv,"k:p:rw");
      if(c==EOF) break;

      switch (c) {
	case 'r':
	  break;
	case 'w':
	  read = 0;
	  break;
	case 'k':
	  key = atoi (optarg);
	  break;
	case 'p':
	  prio = atoi (optarg);
	  break;
	default:
	  printf ("unknown option -%c\n", c);
	  exit (1);
      }
    }
    if (!read) {
      if (argc == optind) {
	line = (char *)malloc (MSGSIZE);
	if (!line) {
	  perror ("malloc");
	  exit (1);
	}
	printf ("Give the message: ");
	fflush (stdout);
	fgets (line, MSGSIZE, stdin);
      } else line = argv [optind];
    }
  } while (0);

  fd = shmget (key, SIZE, IPC_CREAT | S_IRWXU);
  if (fd < 0) {
    perror ("shmget");
    exit (1);
  }
  shm = shmat (fd, NULL, 0);
  if (!shm) {
    perror ("shmat");
    exit (1);
  }

  if (read) {
    info = shm;
    line = (char *)shm + INFOSIZE;
    if (!info->present) { 
      printf ("No message present\n");
      return 0;
    }
    printf ("Message with %d priority dated %s", info->priority, ctime (&info->date));
    fputs (line, stdout);
    if (line[info->size - 1] != '\n') printf ("\n");
  } else {
    char *linedest =  (char *)shm + INFOSIZE;
    int size = strlen (line) + 1;
    info = shm;
    time (&info->date);
    info->present = 1;
    info->priority = prio;
    info->size = (size < MSGSIZE)? size: MSGSIZE;
    memcpy (linedest, line, info->size);
    linedest[info->size] = 0;
  }
  
  return 0;
}
