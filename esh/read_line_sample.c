/*
 *
 * libesh_base: read_line.c
 *
 * $Id: read_line_sample.c,v 1.2 2007-01-18 12:52:42 alessandro Exp $
 *
 * Copyright (C) 2001 Alessandro Razeto
 * Author: Alessandro Razeto <eto@linux.it>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * esh shell base library
 *
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include "esh_base.h"

#include "global.h"

static void strip_blanks (char *line) {
  int index;
  int lenght = strlen (line);
  for (index = 0; index < lenght; index++) 
    if (!isspace (line[index])) break;
  if (index) {
    if (index != (lenght - 1)) 
      memmove (line, line + index, lenght - index + 1);
    else line[0] = 0;
  }
}

#ifdef HAVE_READLINE
#include <readline/readline.h>
#include <readline/history.h>

static const char *def_prompt =  ">";
static char *prompt = 0;

int set_prompt (char *data) {
  
  if (prompt != def_prompt && prompt) free (prompt);
  
  if (!data)
    prompt = (char *)def_prompt;
  else {
    prompt =   (char *)malloc (strlen (data) + 1);
    if (!prompt)
    return SYS_ERROR ("malloc (%d)", strlen (data));
    strcpy (prompt, data);
  }

  return 0;
}

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
int history_open (void) {
  char *home; 
  char file[2000];
  /* check if $HOME/.esh_history exist */
  /* if don't create if */

  return read_history (file);
}

int history_close (int max_entries) {
  char *home; 
  char file[2000];
  /* get the filename of $HOME/.esh_history */
  
  append_history (max_entries, file);
  return history_truncate_file (file, max_entries);
}
  
  
char * read_line (void) {
  /* CALL readline */
  if (!line) return NULL;
  strip_blanks (line);
  /* add to history */
  return line;
}
#else

int set_prompt (char *data) { return 0; }  
int history_open (void) { return 0; }
int history_close (int max_entries) { return 0; }
char * read_line (void) {
  char *line, *ptr;
  line = (char *)malloc(MAX_COMMAND_LENGTH);
  if (!line) return (char*) SYS_ERROR ("malloc (%d)", MAX_COMMAND_LENGTH);
  ptr = fgets (line, MAX_COMMAND_LENGTH, stdin);
  if (!ptr) {
    free (line);
    return NULL;
  }
  ptr = index (line, '\n');
  if (ptr) *ptr = 0;
  strip_blanks (line);
  return line;
}  
#endif  
