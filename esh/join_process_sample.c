/*
 *
 * libesh_base: join_process.c
 *
 * $Id: join_process_sample.c,v 1.3 2007-01-18 12:52:42 alessandro Exp $
 *
 * Copyright (C) 2001 Alessandro Razeto
 * Author: Alessandro Razeto <eto@linux.it>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * esh shell base library
 * sample file
 *
*/
#include <sys/types.h>
#include <unistd.h>
#define _USE_BSD
#include <sys/types.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/wait.h>

#include "esh_base.h"

#include "global.h"

int call_wait (pid_t pid, int flags) {
  int status;
  int id;
  /* CALL waitpid */

  /* CHECK for errors  or for 0 (see manpage) */

  id = find_process(pid);
  /* CHECK if stopped */
  if (...) {
    set_foreground (getpid ());
    add_process (pid, STOPPED_S);
    id = find_process(pid);
    MSG ("Stopped process %d [%d]\n", pid, id);
    return pid;
  }
  if (id < 0) { 
    set_foreground (getpid ());
    /* CHECK if signaled  and print something */
    if (...) 
      MSG ("Process %d signaled with %d\n", pid, ...);
  } else {
    del_process (pid);
    /* CHEK why the process terminated and print something */
    if (...) 
      MSG ("Process %d [%d] exited with %d\n", pid, id, ...);
    if (...) 
      MSG ("Process %d [%d] signaled with %d\n", pid, id, ...);
  }
  return pid;
}



pid_t join (pid_t pid, int block) {
  int id;

  if (pid && block) block =  0;
  else block = WNOHANG;
  if (!pid) pid = -1;
  while (1) {
    id = call_wait (pid, block);
    if (id > 0) break;  // simplest condition
    else if (id < 0 && errno == ECHILD) break;  // no child is present (more)
    else if (!id && block) break; // block has inverted logic here
  }
  return 0;
}

