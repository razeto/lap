/*
 *
 * libesh_base: signals.c
 *
 * $Id: signals.c,v 1.7 2007-01-18 12:52:42 alessandro Exp $
 *
 * Copyright (C) 2001 Alessandro Razeto
 * Author: Alessandro Razeto <eto@linux.it>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * esh shell base library
 *
*/
#include <stdlib.h>
#include <signal.h>
#include <sys/types.h>
#include <unistd.h>

#include "esh_base.h"

#include "global.h"

#ifdef HAVE_READLINE
#include <readline/readline.h>
#include <readline/history.h>
#endif

static void sig_handler (int signo, siginfo_t * info, void * data) {
  int signal_id;
  if (!info) return;   
  signal_id = info->si_signo;
  if (signal_id == SIGINT) {
    readline_signal_clearline ();
    return;
  }
  if (signal_id == SIGUSR1) {
    MSG("Signal SIGUSR1\n");
    return;
  }
  if (signal_id == SIGCHLD) {
    join(0, 0);
    return;
  }
  if (info->si_code == SI_USER) {
    if (info->si_pid == getpid())
      RNT_ERROR ("Raised with %d", signal_id);
    else 
      RNT_ERROR ("Killed by %d with %d", info->si_pid, signal_id);
    set_foreground (getpid ());
    readline_signal_finish ();
    exit (1);
  }
}
  
int register_signal (int signal_id, void (*handler)(int, siginfo_t *, void *), int sys_cont) {
  struct  sigaction  act;
  act.sa_sigaction = handler;
  act.sa_flags = SA_RESTART | SA_SIGINFO;
  sigemptyset(&(act.sa_mask));
  if (sigaction (signal_id, &act, NULL) < 0)
    return SYS_ERROR ("sigaction (%d)", signal_id);
  return 0;
}

int set_signals (void) {
  readline_builtin_signals (0);
#define REG(_s)	if (register_signal (_s, sig_handler, 1) < 0) return -1
  REG (SIGINT);
  REG (SIGQUIT);
  REG (SIGABRT);
  REG (SIGTERM);
  REG (SIGCHLD);
  REG (SIGTSTP);
  REG (SIGUSR1);
  REG (SIGTTOU);
  return 0;
}
