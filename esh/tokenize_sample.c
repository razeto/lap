/*
 *
 * libesh_base: tokenize.c
 *
 * $Id: tokenize_sample.c,v 1.2 2007-01-18 12:52:44 alessandro Exp $
 *
 * Copyright (C) 2001 Alessandro Razeto
 * Author: Alessandro Razeto <eto@linux.it>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * esh shell base library
 * sample file
 *
*/
#include <stdlib.h>
#include <stdio.h>

#include "esh_base.h"

#include "global.h"

int free_tokens(char *tokens[])  {
  int i = 0;
  for (;;i++) { 
    if (!tokens[i]) break;
    else free (tokens[i]);
  }
  free (tokens);
  
  return 0;
}

int tokenize (const char *line, char **tokens[]) {

  /* Declare stack varibles (normally called automatic) */
  
  
  
  *tokens = (char **)calloc (MAX_COMMAND_LENGTH, sizeof (char));
  if (!tokens) 
    return SYS_ERROR ("calloc (%d)", MAX_COMMAND_LENGTH);
  
  
  for (;;i++) {
    /* Create tokens */
    ptr = strtok (...)
    if (!ptr) break;
    size = strlen (ptr) + 1; /* include the \0 */
    /* allocate memory for tokens[i] */
    (*tokens)[i] = 
    if (!(*tokens)[i]) 
      return SYS_ERROR ("malloc (%d)", size);
    memcpy ((*tokens)[i], ptr, size); 
  }
  return i;
}
