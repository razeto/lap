/*
 *
 * libesh_base: terminal_control.c
 *
 * $Id: terminal_control.c,v 1.4 2007-01-18 12:52:44 alessandro Exp $
 *
 * Copyright (C) 2001 Alessandro Razeto
 * Author: Alessandro Razeto <eto@linux.it>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * esh shell base library
 *
*/
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>

#include "esh_base.h"

#include "global.h"

int set_foreground (pid_t pid) {
  int b = 0;
  sigset_t set, oset;
  sigemptyset (&set);
  sigaddset (&set, SIGTTOU);
  sigaddset (&set, SIGTTIN);
  sigaddset (&set, SIGTSTP);
  sigaddset (&set, SIGCHLD);
  sigemptyset (&oset);
  sigprocmask (SIG_BLOCK, &set, &oset);
  if (!kill (pid, 0)) /* check that the pid already exists */
    b = tcsetpgrp (0, pid);
  sigprocmask (SIG_SETMASK, &oset, (sigset_t *)NULL);
  if (b < 0) return SYS_ERROR ("tcsetpgrp (0, %d)", pid);
  return 0;
}
