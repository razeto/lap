#ifndef _GLOBAL_H
#define _GLOBAL_H


#define HAVE_READLINE



#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#define FAIL -1
#define MSG(fmt, args...)       fprintf(stderr, fmt, ##args)
#define RNT_ERROR(fmt, args...) ({ \
    fprintf(stderr, "!!! %s in %s,%d >> ", __PRETTY_FUNCTION__, __FILE__, __LINE__); \
    fprintf(stderr, fmt "\n", ##args); \
    FAIL; })
#define SYS_ERROR(fmt, args...) ({ \
    fprintf(stderr, "!!! %s in %s,%d >> ", __PRETTY_FUNCTION__, __FILE__, __LINE__); \
    fprintf(stderr, fmt, ##args); \
    fprintf(stderr, ": %s (%d)\n", strerror(errno), errno);  \
    FAIL; })



#endif
