/*
 *
 * libesh_base: esh_base.h
 *
 * $Id: esh_base.h,v 1.7 2007-01-18 12:52:41 alessandro Exp $
 *
 * Copyright (C) 2001 Alessandro Razeto
 * Author: Alessandro Razeto <eto@linux.it>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * esh shell base library
 *
*/
#ifndef _ESH_H
#define _ESH_H

#include <sys/types.h>

#define VERSION "0.1"

pid_t spawn_function(void (*function)(void *), void * data);

pid_t join (pid_t id, int block);

pid_t spawn_program (char *command[]);

#define MAX_COMMAND_LENGTH 256
int exec_call (char *binary, char *argv[]);

int free_tokens(char *tokens[]);
int tokenize (const char *line, char **tokens[]);

int set_prompt (char *data);
char * read_line (void);
int history_open (void);
int history_close (int max_entries);
void readline_builtin_signals (int logic);
void readline_signal_clearline (void);
void readline_signal_finish (void);
void readline_signal_reinit (void);

#define MAX_DETACHED 32
#define RUNNING_S 0
#define RUNNING_STR "Running"
#define STOPPED_S 1
#define STOPPED_STR "Stopped"
#define DIED_S 2
#define DIED_STR "Died"
#define WAITING_S 3
#define WAITING_STR "Waiting"
int detached_queue_init (void);
void list_entries (void);
int find_process (pid_t pid);
pid_t find_id (int id);
int add_process (pid_t pid, int status);
void del_process (pid_t pid);
void change_process_status (pid_t pid, int status);
void change_id_status (int id, int status);
int get_status (int id);

#include <signal.h>
int register_signal (int signal_id, void (*handler)(int, siginfo_t *, void *), int sys_cont);
int set_signals (void);

int fg (void);
int execute (void);
int parse (char *tokens[]);

int set_foreground (pid_t pid);

void logout (void);
#endif
