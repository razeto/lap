/*
 *
 * libesh_base: parse.c
 *
 * $Id: parse_sample.c,v 1.1 2002-03-02 15:35:34 razeto Exp $
 *
 * Copyright (C) 2001 Alessandro Razeto
 * Author: Alessandro Razeto <eto@linux.it>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * esh shell base library
 *
*/
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <signal.h>

#include "esh_base.h"

static int _execute = 0;
static int _bg = 0;

int fg (void) { return !_bg; }
int execute (void) { return _execute; } 
int parse (char *tokens[]) {
  int num = -1;
  while (tokens[num++]) ;
  num--;
  _bg = _execute = 0;
  
  if (!strcmp (tokens[0], "jobs")) { 
    list_entries();
    return 0;
  }
  if (num > 1 && !strcmp (tokens[num-1], "&")) { 
    _execute = _bg = 1;
    free (tokens[num-1]);
    tokens[num-1] = NULL;
    return 0;
  }
  if (!strcmp (tokens[0], "fg")) {
    int job = 0;
    pid_t pid;
    if (num > 1)  job = atoi (tokens[1]);
    pid = find_id (job);
    if (get_status (job) == STOPPED_S) 
      kill (pid, SIGCONT);
    del_process (pid); 
    join (pid, NULL);
    return 0;
  }
  if (!strcmp (tokens[0], "bg")) {
    int job = 0;
    pid_t pid;
    if (num > 1)  job = atoi (tokens[1]);
    pid = find_id (job);
    change_id_status (job, RUNNING_S);
    kill (pid, SIGCONT);
    return 0;
  }
  
  _execute = 1;
  return 0;   
}
