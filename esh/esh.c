/*
 *
 * libesh_base: main.c
 *
 * $Id: esh.c,v 1.7 2007-01-18 12:52:41 alessandro Exp $
 *
 * Copyright (C) 2001 Alessandro Razeto
 * Author: Alessandro Razeto <eto@linux.it>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * esh shell base library
 *
*/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>


#include "esh_base.h"


int main (int argc, char *argv[]) {
  
  fprintf (stderr, "Eto shell v%s started with pid %d\n", VERSION, getpid());
  set_prompt ("@");
  history_open();
  detached_queue_init ();
  set_signals ();
  
  
  setpgid(0, getpid ());
  set_foreground (getpid ());

  while (1) {
    char **tokens;
    int ntokens;
    char *line = read_line();
    pid_t pid;
    if (!line) break;
    ntokens = tokenize (line, &tokens);
    if (ntokens > 0) {
      parse (tokens);
      if (execute ()) { 
	pid = spawn_program (tokens);
        if (fg ()) {
	  set_foreground (pid);
	  join (pid, 1);
	} else {
	  int id = add_process (pid, RUNNING_S);
	  fprintf (stderr, "Process '%s' %d [%d]\n", tokens[0], pid, id);
	}
      }
      free_tokens (tokens);
    }
    free (line);

  }
  logout ();
  return 0;
}




