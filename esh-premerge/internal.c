/*
 *
 * libesh_base: internal.c
 *
 * $Id: internal.c,v 1.1 2004-02-22 18:35:05 razeto Exp $
 *
 * Copyright (C) 2001 Alessandro Razeto
 * Author: Alessandro Razeto <eto@linux.it>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * esh shell base library
 *
*/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>

#define _INTERNAL_C
#include "esh_base.h"
#include "internal.h"

#ifdef _USE_GLOBAL
#include "global.h"
#else
#include <string.h>
#include <errno.h>
#include <stdio.h>
#define MSG(fmt, args...)       fprintf(stderr, fmt, ##args)
#define FAIL -1 
#define SYS_ERROR(fmt, args...) ({ \
    fprintf(stderr, "!!! %s in %s,%d >> ", __PRETTY_FUNCTION__, __FILE__, __LINE__); \
    fprintf(stderr, fmt, ##args); \
    fprintf(stderr, ": %s (%d)\n", strerror(errno), errno);  \
    FAIL; })
#endif


#include "variable_mngr.h"


void internal_cd (char *where) {
  char *dir;
  const char *last_dir = get_var_value ("OLDPWD");
  const char *cwd = get_var_value ("PWD");

  if (!where)  dir = (char *)get_var_value ("HOME");
  else if (!strcmp (where, "-")) {
    last_dir = get_var_value ("OLDPWD");
    if (last_dir[0]) {
      dir = strdup (last_dir);
      if (!dir) { 
	SYS_ERROR ("strdup");
	return;
      }
    }
    else { 
      MSG ("OLDPWD not defined\n");
      return;
    }
  }
  else dir = where;
  
  
  if (chdir (dir) < 0) {
    if (errno == ENAMETOOLONG || errno == ENOENT || errno == ENOTDIR || 
	errno == EACCES || errno == ELOOP || errno == EIO) 
      MSG ("cd %s: %s\n", dir, strerror(errno));
    else SYS_ERROR ("chdir(%s)", dir);
  }
  else {
    set_var_value ("OLDPWD", cwd);
    set_var_value ("PWD", dir);
  }
  return;
}



static int find_job (char *id) {
  int job = 0;
  pid_t pid;
  if (id) {
    if (id[0] == '%') {
      job = atoi (id + 1);
      pid = find_id (job);
      if (pid < 0) {
	printf ("Id process %%%d not present\n", job);
	return -1;
      }
    } else {
      pid = atoi (id);
      job = find_process (pid);
      if (job < 0) {
	printf ("Process %d not present\n", pid);
	return -1;
      }
    }
  } else {
    pid = find_id (job);
    if (pid < 0) {
      printf ("No job present\n");
      return -1;
    }
  }
  return job;
}
  

void internal_fg (char *id) {
  pid_t pid, gid; 
  int job = find_job (id);
  if (job < 0) return;
  pid = find_id (job);
  gid = getpgid (pid);
  if (get_status (job) == STOPPED_S) 
    kill (-1 * gid, SIGCONT);
  del_process (pid); 
  set_foreground (gid);
  join (pid, 1);
  return;
}

void internal_bg (char *id) {
  pid_t pid; 
  int job = find_job (id);
  if (job < 0) return;
  pid = find_id (job);
  change_id_status (job, RUNNING_S);
  kill (pid, SIGCONT);
  return;
}


void internal_raise (char *signal) {
  int sig = 15;
  if(signal) {
    int sig_2 = atoi (signal);
    /* CALL SOMETHING TO ... */
    if (sig_2 > 0) sig = sig_2;
  } 
  raise(sig);
  return;
}

