/*
 *
 * libesh_base: spawn_function.c
 *
 * $Id: spawn_function.c,v 1.4 2004-02-22 18:35:05 razeto Exp $
 *
 * Copyright (C) 2001 Alessandro Razeto
 * Author: Alessandro Razeto <eto@linux.it>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * esh shell base library
 *
*/
#include <sys/types.h>
#include <unistd.h>
#define _USE_BSD
#include <sys/types.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/wait.h>

#include "esh_base.h"

#ifdef _USE_GLOBAL
#include "global.h"
#else
#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#define FAIL -1 
#define SYS_ERROR(fmt, args...) ({ \
    fprintf(stderr, "!!! %s in %s,%d >> ", __PRETTY_FUNCTION__, __FILE__, __LINE__); \
    fprintf(stderr, fmt, ##args); \
    fprintf(stderr, ": %s (%d)\n", strerror(errno), errno);  \
    FAIL; })
#define MSG(fmt, args...)       fprintf(stderr, fmt, ##args)
#endif


pid_t spawn_function(void (*function)(void *), void * data, pid_t gid, int fg) {
  pid_t pid;
  
  if (!function || !data) return -1;

  pid = fork ();
  if (pid < 0) return SYS_ERROR ("fork");

  if (!pid) {
    setpgid (getpid (), gid);
    if (fg) { 
      if (!gid) gid = getpid();
      set_foreground (gid);
    }
    close_fd_copies();
    (*function)(data);
    MSG ("The user function did not exited\n");
    exit (1);
  }
  
  setpgid (pid, gid);
  if (fg) { 
    if (!gid) gid = pid;
    set_foreground (gid);
  }
  return pid;
}

