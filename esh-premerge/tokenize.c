/*
 *
 * libesh_base: tokenize.c
 *
 * $Id: tokenize.c,v 1.4 2004-02-22 18:35:05 razeto Exp $
 *
 * Copyright (C) 2001 Alessandro Razeto
 * Author: Alessandro Razeto <eto@linux.it>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * esh shell base library
 *
*/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "esh_base.h"

#ifdef _USE_GLOBAL
#include "global.h"
#else
#include <string.h>
#include <errno.h>
#include <stdio.h>
#define FAIL -1
#define SYS_ERROR(fmt, args...) ({ \
    fprintf(stderr, "!!! %s in %s,%d >> ", __PRETTY_FUNCTION__, __FILE__, __LINE__); \
    fprintf(stderr, fmt, ##args); \
    fprintf(stderr, ": %s (%d)\n", strerror(errno), errno);  \
    FAIL; })
#define RNT_ERROR(fmt, args...) ({ \
    fprintf(stderr, "!!! %s in %s,%d >> ", __PRETTY_FUNCTION__, __FILE__, __LINE__); \
    fprintf(stderr, fmt "\n", ##args); \
    FAIL; })
#define MSG(fmt, args...)       fprintf(stderr, fmt, ##args)
#endif

int free_tokens(char *tokens[])  {
  int i = 0;
  for (;;i++) { 
    if (!tokens[i]) break;
    else free (tokens[i]);
  }
  free (tokens);
  
  return 0;
}

int tokenize (const char *line, char **tokens[]) {
  int i = 0;
  static char line_local[MAX_COMMAND_LENGTH];
  char *ptr;
  int size;
  
  size = strlen (line);
  if (!size) return 0;
  if (size >= MAX_COMMAND_LENGTH) {
    MSG ("Line too long\n");
    return 0;
  }
  memcpy (line_local, line, size + 1);
    
  *tokens = (char **)calloc (MAX_COMMAND_LENGTH, sizeof (char));
  if (!tokens) 
    return SYS_ERROR ("calloc (%d)", MAX_COMMAND_LENGTH);
  
  
  for (;;i++) {
    ptr = strtok ((!i) ? line_local : NULL, " \v\t\n");
    if (!ptr) break;
    size = strlen (ptr) + 1; /* include the \0 */
    (*tokens)[i] = (char *)malloc (size);
    if (!(*tokens)[i]) 
      return SYS_ERROR ("malloc (%d)", size);
    memcpy ((*tokens)[i], ptr, size); 
  }
  return i;
}

int add_token (const char *token, char **tokens[]) {
  int i;
  if (!*tokens) {
    *tokens = (char **)calloc (MAX_COMMAND_LENGTH, sizeof (char));
    if (!tokens) 
      return SYS_ERROR ("calloc (%d)", MAX_COMMAND_LENGTH);
  }
  for (i = 0; i < MAX_COMMAND_LENGTH; i ++) if (!(*tokens)[i]) break;
  if (i == MAX_COMMAND_LENGTH) return RNT_ERROR ("line too long");

  (*tokens)[i] = (char *)token;

  return i;
}

int glue_tokens (const char *token, char **line) {
  int len1, len2;
  if (!*line) {
    *line = strdup (token);
    if (!*line) return SYS_ERROR ("strdup");
    return 0;
  }
  len1 = strlen (*line);
  len2 = strlen (token);

  *line = (char *)realloc (*line, len1+len2+1);
  memcpy (*line + len1, token, len2 + 1);
  return 0;
}
