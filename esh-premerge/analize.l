/*
 *
 * libesh_base: analyze.l
 *
 * $Id: analize.l,v 1.2 2004-02-22 18:35:05 razeto Exp $
 *
 * Copyright (C) 2001 Alessandro Razeto
 * Author: Alessandro Razeto <eto@linux.it>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * esh shell base library
 *
*/
%{
#include <string.h>
#include <stdio.h>
#include "esh_base.h"
#include "variable_mngr.h"
#include "parse.tab.h"

#ifdef _USE_GLOBAL
#include "global.h"
#else
#include <string.h>
#include <errno.h>
#include <stdio.h>
#define MSG(fmt, args...)       fprintf(stderr, fmt, ##args)
#define FAIL -1 
#define SYS_ERROR(fmt, args...) ({ \
    fprintf(stderr, "!!! %s in %s,%d >> ", __PRETTY_FUNCTION__, __FILE__, __LINE__); \
    fprintf(stderr, fmt, ##args); \
    fprintf(stderr, ": %s (%d)\n", strerror(errno), errno);  \
    FAIL; })
#define RNT_ERROR(fmt, args...) ({ \
    fprintf(stderr, "!!! %s in %s,%d >> ", __PRETTY_FUNCTION__, __FILE__, __LINE__); \
    fprintf(stderr, fmt "\n", ##args); \
    FAIL; })
#endif

int get_str (char *dest, int max); 
char *quoteblock_handle (char *str);
int arrange_backquotes (void);
int replace_variable (void);
int replace_str (char **base, int offset, int n, const char *with);
#define UNLIMITED			-1
inline int copy (int len) {
  extern char *strndup(const char *, size_t);
  if (len == UNLIMITED) yylval.str = strdup (yytext);
  else yylval.str = strndup (yytext, len);
  if (!yylval.str) return SYS_ERROR ("strdup");
  return 0;
}
#define HANDLE_STR()			do { copy(UNLIMITED); if (!replace_variable ()) \
					      return arrange_backquotes (); } while (0)
#define HANDLE_ASSIGNEMENT()		do { int len = strcspn (yytext, " \t\v\n="); \
					  copy (len); unput('='); return STR; } while (0)

#define YY_DECL 			int yylex (void)
#define YY_INPUT(buf, result, max_size) { result = get_str (buf, max_size); }
%}
	static int line_offset = 0;
	static int line_lenght = 0;
	static char *line_base = NULL;

%option noyywrap
%array
%x quote

STR 	[^;=\n\ \t\v&"|><*\\\003$]
QUOTED	\\[^\n]
SPACE	[ \t\v]
NUM	[0-9]
%%
<*>\003			{ BEGIN (0); set_prompt ("@");  yylval.str = "CTLC"; return CTLC; }
{SPACE}+		return SPACES;
\n			{ set_prompt ("@"); return '\n'; }
\\\n			{ set_prompt (">"); }
{NUM}*>			{ yylval.num = (yytext[0] == '>') ? 1 : atoi (yytext); return REDIR_OUT_NUM; }
{NUM}*>{2}		{ yylval.num = (yytext[0] == '>') ? 1 : atoi (yytext); return REDIR_OUT_APPEND_NUM; }
{NUM}*<			{ yylval.num = (yytext[0] == '>') ? 0 : atoi (yytext); return REDIR_IN_NUM; }
jobs			return JOBS_STR;
logout|exit		return LOGOUT_STR;
suspend			return SUSPEND_STR;
raise			return RAISE_STR;
cd			return CD_STR;
fg			return FG_STR;
bg			return BG_STR;
unset			return UNSET_STR;
environment		return ENV_STR;
export			return EXPORT_STR;
({STR}|{QUOTED}|(\${STR})|\${2})+ HANDLE_STR();
{STR}+{SPACE}*=		HANDLE_ASSIGNEMENT();
\"			{ BEGIN (quote); quoteblock_handle ((char *)-1); }
<quote>{
\n			{ quoteblock_handle (yytext); set_prompt (">"); }
[^"\n]*			{ quoteblock_handle (yytext); }	
\"			{ set_prompt ("@"); yylval.str = quoteblock_handle (NULL); BEGIN (0); return QUOTE_STR; }
}
.			return yytext[0];
%%


#define MIN(a,b)	((a) < (b))? (a) : (b)
int get_str (char *dest, int max) {
  int maxlen;
  if (max < 1) return YY_NULL;
  if (yyin == stdin) { 
    if (!line_base) {
      line_base = read_line();
      if (!line_base) return YY_NULL;
      line_lenght = strlen (line_base);
      if (async_char) {
	dest[0] = async_char;
	async_char = 0;
	return 1;
      }
    }
    if (line_offset == line_lenght) {
      free (line_base);
      line_offset = line_lenght = 0;
      line_base = NULL;
      dest[0] = '\n';
      return 1;
    }
    maxlen = MIN (line_lenght - line_offset, max);
    memcpy (dest, line_base + line_offset, maxlen);
    line_offset += maxlen;
  } else {
    maxlen = fread(dest, 1, max, yyin);
    if (!maxlen) { 
      if (ferror(yyin)) YY_FATAL_ERROR ("input in flex scanner failed");
      return YY_NULL;
    }
  }
  return maxlen;
}

char *quoteblock_handle (char *str) {
  static char *ptr = 0;
  if (!str) return ptr;
  if (str == (char *)-1) return ptr = NULL;
  if (!ptr) ptr = strdup (str);
  else {
    int len1 = strlen (ptr);
    int len2 = strlen (str);
    ptr = realloc (ptr, len1 + len2 + 1);
    if (!ptr) return ptr;
    memcpy (ptr + len1, str, len2);
    ptr [len1 + len2] = 0;
  }
  return ptr;
}


int arrange_backquotes (void) {
  char *bq = yylval.str;
  int len = strlen (bq);
  char *max = bq + len;

  while (1) {
    bq = index (bq, '\\');
    if (!bq) break;
    if (bq[1] == '\\') bq++;
    memmove (bq, bq + 1, max-- - bq);
  }
  return  STR;
}


int replace_variable (void) {
  char *base = yylval.str;
  char *dollar = base;
  char *tmp;
  const char *varcontent;
  int varsize;
  int found = 0;
  int quoted = 0;
  
  while (1) {
    tmp = dollar = index (dollar, '$');
    if (!dollar) break;
    while (tmp-- > base) {
      if (*tmp == '\\') quoted = !quoted;
      else break;
    }
    if (!quoted) {
      found = 1;
      varcontent = find_variable (dollar, &varsize);
      replace_str (&base, dollar - base, varsize, varcontent);
    }
    dollar++;
  }
  
  if (!found) return 0;
  else {
    int i = strlen (base);
    for (; i>0; i--) unput (base[i - 1]);
    return 1;
  }
}

int replace_str (char **base, int offset, int nbyte, const char *with) {
  int len = with ? strlen (with) : 0;
  int totlen = strlen (*base);
  char *oldpos = *base + offset + nbyte;
  char *newpos = *base + offset + len;
  if (len > nbyte) {
    *base = realloc (*base, totlen - nbyte + len + 1);
    if (!*base) return FAIL;
  }
  memmove (newpos, oldpos,  totlen - (offset + nbyte) + 1);
  if (len) memcpy (*base + offset, with, len);
  return 1;
}
