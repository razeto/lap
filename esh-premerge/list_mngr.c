/*
 *
 * libesh_base: list_mngr.c
 *
 * $Id: list_mngr.c,v 1.2 2002-03-13 20:27:42 razeto Exp $
 *
 * Copyright (C) 2001 Alessandro Razeto
 * Author: Alessandro Razeto <eto@linux.it>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * esh shell base library
 *
*/
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>

#include "esh_base.h"
#ifdef _USE_GLOBAL
#include "global.h"
#else
#include <string.h>
#include <errno.h>
#include <stdio.h>
#define FAIL -1
#define SYS_ERROR(fmt, args...) ({ \
    fprintf(stderr, "!!! %s in %s,%d >> ", __PRETTY_FUNCTION__, __FILE__, __LINE__); \
    fprintf(stderr, fmt, ##args); \
    fprintf(stderr, ": %s (%d)\n", strerror(errno), errno);  \
    FAIL; })
#define MSG(fmt, args...)       fprintf(stderr, fmt, ##args)
#endif


struct dprocess {
  pid_t pid;
  int status;
};

static struct dprocess * db = NULL;
static int entries = 0; 
static char *msg[4];

int detached_queue_init (void) {
  db = calloc (MAX_DETACHED, sizeof (struct dprocess));
  if (!db) return SYS_ERROR("calloc(%d, %d)", MAX_DETACHED, sizeof (struct dprocess));
  msg[STOPPED_S] = STOPPED_STR;
  msg[RUNNING_S] = RUNNING_STR;
  msg[DIED_S] = DIED_STR;
  msg[WAITING_S] = WAITING_STR;
  return 0;
}

void list_entries (void) {
  int i = 0;
  MSG ("Process table\n");
  for (; i < entries; i++)  MSG ("   %4d  [%d] %s\n" , db[i].pid, i, msg[db[i].status]);
}
int find_process (pid_t pid) {
  int i = 0;
  for (; i < entries; i++)
    if (db[i].pid == pid) return i;
  return -1;
}
pid_t find_id (int id) {
  if (id >= entries) return -1;
  return db[id].pid;
}
int add_process (pid_t pid, int status) {
  int id = find_process (pid);
  if (id >= 0) {
    db[id].status = status;
    return id;
  }
  db[entries].pid = pid;
  db[entries].status = status;
  return entries ++;
}
void change_process_status (pid_t pid, int status) {
  int index = find_process (pid);
  if (index < 0) return;
  db[index].status = status;
}
void change_id_status (int id, int status) {
  if (id >= entries) return;
  db[id].status = status;
}
  
void del_process (pid_t pid) {
  int index = find_process (pid);
  if (index < 0) return;
  if (index != (entries - 1)) 
    memmove (&db[index], &db[index+1], sizeof (struct dprocess) * (entries - 1 - index));
  entries --;  
}

int  get_status (int id) {
  if (id >= entries) return -1;
  return db[id].status;
}
