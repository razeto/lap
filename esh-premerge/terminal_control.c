/*
 *
 * libesh_base: terminal_control.c
 *
 * $Id: terminal_control.c,v 1.3 2006-06-02 16:55:19 razeto Exp $
 *
 * Copyright (C) 2001 Alessandro Razeto
 * Author: Alessandro Razeto <eto@linux.it>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * esh shell base library
 *
*/
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>

#include "esh_base.h"

#ifdef _USE_GLOBAL
#include "global.h"
#else
#include <string.h>
#include <errno.h>
#include <stdio.h>
#define FAIL -1 
#define SYS_ERROR(fmt, args...) ({ \
    fprintf(stderr, "!!! %s in %s,%d >> ", __PRETTY_FUNCTION__, __FILE__, __LINE__); \
    fprintf(stderr, fmt, ##args); \
    fprintf(stderr, ": %s (%d)\n", strerror(errno), errno);  \
    FAIL; })
#define MSG(fmt, args...)       fprintf(stderr, fmt, ##args)
#endif



int copy_fds (void) {
  if (dup2 (0, STDIN_COPY) < 0) 
    return SYS_ERROR("dup2 (0, STDIN_COPY=%d)", STDIN_COPY);
  if (dup2 (1, STDOUT_COPY) < 0) 
    return SYS_ERROR("dup2 (1, STDOUT_COPY=%d)", STDOUT_COPY);
  if (dup2 (2, STDERR_COPY) < 0) 
    return SYS_ERROR("dup2 (2, STDERR_COPY=%d)", STDERR_COPY);
  return 0;
}

int restore_fds (void) {
  if (dup2 (STDIN_COPY, 0) < 0) 
    return SYS_ERROR("dup2 (STDIN_COPY=%d, 0)", STDIN_COPY);
  if (dup2 (STDOUT_COPY, 1) < 0) 
    return SYS_ERROR("dup2 (STDOUT_COPY=%d, 1)", STDOUT_COPY);
  if (dup2 (STDERR_COPY, 2) < 0) 
    return SYS_ERROR("dup2 (STDERR_COPY=%d, 2)", STDERR_COPY);
  return 0;
}

void close_fd_copies (void) {
  close (STDIN_COPY);
  close (STDOUT_COPY);
  close (STDERR_COPY);
}

int set_foreground (pid_t pid) {
  int b = 0;
  sigset_t set, oset;
  sigemptyset (&set);
  sigaddset (&set, SIGTTOU);
  sigaddset (&set, SIGTTIN);
  sigaddset (&set, SIGTSTP);
  sigaddset (&set, SIGCHLD);
  sigemptyset (&oset);
  sigprocmask (SIG_BLOCK, &set, &oset);
  if (!kill (pid, 0)) /* check that the pid already exists */
    b = tcsetpgrp (STDIN_COPY, pid);
  sigprocmask (SIG_SETMASK, &oset, (sigset_t *)NULL);
  if (b < 0) return SYS_ERROR ("tcsetpgrp (0, %d)", pid);
  return 0;
}
