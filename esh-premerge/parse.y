/*
 *
 * libesh_base: parse.y
 *
 * $Id: parse.y,v 1.2 2004-02-22 18:35:05 razeto Exp $
 *
 * Copyright (C) 2001 Alessandro Razeto
 * Author: Alessandro Razeto <eto@linux.it>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * esh shell base library
 *
*/
%{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "esh_base.h"
#include "internal.h"
#include "small_functions.h"
#include "variable_mngr.h"

int yylex (void);
void yyerror (char *);
%}

%union {
  char *str;
  struct cmd *cmd;
  struct varlist *vlist;
  int num;
  void *nil;
}

/* TERMINAL SIMBOLS */
%token  <nil> SPACES
%token	<nil> CTLC
%token	<nil> LOGOUT_STR
%token	<nil> SUSPEND_STR
%token	<nil> RAISE_STR
%token	<nil> JOBS_STR
%token	<nil> CD_STR
%token	<nil> FG_STR
%token	<nil> BG_STR
%token  <nil> ENV_STR
%token  <nil> UNSET_STR
%token	<nil> EXPORT_STR
%token	<nil> '&'
%token	<nil> '\n'
%token	<str> QUOTE_STR
%token	<str> STR
%token	<num> REDIR_OUT_NUM
%token	<num> REDIR_OUT_APPEND_NUM
%token	<num> REDIR_IN_NUM


%type	<cmd> line
%type	<nil> cmd
%type 	<nil> internal
%type	<nil> cd
%type	<nil> jobs
%type	<nil> logout
%type	<nil> fg
%type	<nil> bg
%type	<nil> suspend
%type	<nil> raise
%type	<nil> unset
%type	<nil> env
%type	<nil> export
%type	<str> str
%type	<str> vdecl
%type	<vlist> vdecls
%type	<vlist> varlist

%%
input:  /* empty */
	| input	eol
	| input vdecls eol	{ commit_vars ($2); }
	| input SPACES vdecls eol	
				{ commit_vars ($3); }
	| input cmd
	| input SPACES cmd
	| input internal
	| input SPACES internal
	| input error eol	{ yyerrok; }
;	
opt_space: 			{ }
	| SPACES		{ }
;
eol:	opt_space '\n'		{ }
	| opt_space ';'		{ }
	| CTLC			{ }
;
str:	STR			{ $$ = $1; }
	| QUOTE_STR		{ $$ = $1; }
;
line:	str			{ $$ = NULL; add_command_token ($1, &($$)); }
	| line SPACES str	{ add_command_token ($3, &($1)); $$ = $1; } 
	| line opt_space REDIR_OUT_NUM opt_space str 	
				{ add_command_redirection ($5, &($1),$3, W); $$ = $1; }
	| line opt_space REDIR_OUT_APPEND_NUM opt_space str 
			    	{ add_command_redirection ($5, &($1),$3, W | A); $$ = $1; }
	| line opt_space REDIR_IN_NUM opt_space str 	
		      		{ add_command_redirection ($5, &($1),$3, R); $$ = $1; }
	| line opt_space '|' opt_space str
	      			{ add_command_pipe ($5, &($1)); $$ = $1; }  
;
cmd:	line eol		{ execute_commad (&($1), 1, 1); }
	| line opt_space '&' eol		
				{ execute_commad (&($1), 0, 0); }
;
cd:	CD_STR eol		{ internal_cd (NULL); }
	| CD_STR SPACES str eol	{ internal_cd ($3); free ($3); }
;
jobs:	JOBS_STR eol		{ internal_jobs (); }
;
logout: LOGOUT_STR eol		{ internal_logout (); }
;
fg:	FG_STR	eol		{ internal_fg (NULL); }
	| FG_STR SPACES STR eol	{ internal_fg ($3); free ($3); }
;
bg:	BG_STR	eol		{ internal_bg (NULL); }
	| BG_STR SPACES STR eol	{ internal_bg ($3); free ($3); }
;
suspend: SUSPEND_STR eol	{ internal_suspend (); }
;
raise:	RAISE_STR eol		{ internal_bg (NULL); }
	| RAISE_STR SPACES str eol	
				{ internal_bg ($3); free ($3); }
;
env:	ENV_STR eol		{ internal_env (NULL); }
	| ENV_STR SPACES str eol	
				{ internal_env ($3); free ($3); }
;
vdecl:	STR '=' str		{ declare_var ($1, $3, 0); $$ = $1; }
	| STR '=' vdecl		{ declare_var ($1, $3, 0); $$ = $1; }
	| STR '=' 		{ declare_var ($1, NULL, 0); $$ = $1; }
;
vdecls: vdecl 			{ $$ = add_var_list ($1, NULL); }
	| vdecls SPACES vdecl	{ $$ = add_var_list ($3, $1); }
;

varlist: str			{ $$ = add_var_list ($1, NULL); }
	| varlist SPACES str	{ $$ = add_var_list ($3, $1); }
;

export:	EXPORT_STR SPACES vdecls eol	
				{ commit_vars (NULL); internal_export ($3); var_list_free ($3); }
	| EXPORT_STR SPACES varlist eol	
				{ internal_export ($3); var_list_free ($3); }
;

unset:	UNSET_STR SPACES varlist eol
				{ internal_unset ($3); var_list_free ($3); }
;

internal: cd | jobs | logout | fg | bg | suspend | raise | env | unset | export
;

%%

void yyerror (char *s) {
  printf ("%s: %s\n", s, yylval.str);
}
  
void bisonclear (void) {
  yyclearin;
}
