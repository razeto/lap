/*
 *
 * libesh_base: signals.c
 *
 * $Id: signals_sample.c,v 1.1 2002-03-02 15:35:34 razeto Exp $
 *
 * Copyright (C) 2001 Alessandro Razeto
 * Author: Alessandro Razeto <eto@linux.it>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * esh shell base library
 * sample file
 *
*/
#include <stdlib.h>
#include <signal.h>
#include <sys/types.h>

#include "esh_base.h"

#ifdef _USE_GLOBAL
#include "global.h"
#else
#include <string.h>
#include <errno.h>
#include <stdio.h>
#define FAIL -1
#define RNT_ERROR(fmt, args...) ({ \
    fprintf(stderr, "!!! %s in %s,%d >> ", __PRETTY_FUNCTION__, __FILE__, __LINE__); \
    fprintf(stderr, fmt "\n", ##args); \
    FAIL; })
#define MSG(fmt, args...)       fprintf(stderr, fmt, ##args) 
#define SYS_ERROR(fmt, args...) ({ \
    fprintf(stderr, "!!! %s in %s,%d >> ", __PRETTY_FUNCTION__, __FILE__, __LINE__); \
    fprintf(stderr, fmt, ##args); \
    fprintf(stderr, ": %s (%d)\n", strerror(errno), errno);  \
    FAIL; })
#endif


static void sig_handler (int signo, siginfo_t * info, void * data) {
  int signal_id;
  if (!info) {
 /* siging not handled */
  return;   
  }
  signal_id = info->si_signo;
//  MSG ("Signal %d process id %d\n", signal_id, info->si_pid);
  if (signal_id == SIGCHLD) {
    if (info->si_code == CLD_KILLED || info->si_code ==  CLD_EXITED || 
	info->si_code == CLD_DUMPED || info->si_code ==  CLD_TRAPPED) {
      if (find_process (info->si_pid) >= 0) join (info->si_pid, 0);
    }
  //  else MSG ("child stopped, ignoring\n");
  }
  if (info->si_code == SI_USER) {
    RNT_ERROR ("Raised");
    exit (1);
  }
}
  
int register_signal (int signal_id, void (*handler)(int, siginfo_t *, void *), int sys_cont) {
  /* FILL it */
}

int set_signals (void) {
  /* register some signals you need */

  return 0;
}
