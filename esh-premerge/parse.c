/*
 *
 * libesh_base: parse.c
 *
 * $Id: parse.c,v 1.5 2002-03-13 20:27:42 razeto Exp $
 *
 * Copyright (C) 2001 Alessandro Razeto
 * Author: Alessandro Razeto <eto@linux.it>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * esh shell base library
 *
*/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <signal.h>
#include <errno.h>
#include <unistd.h>

#include "esh_base.h"

static int _execute = 0;
static int _bg = 0;

int fg (void) { return !_bg; }
int execute (void) { return _execute; } 
int parse (char *tokens[]) {
  int num = -1;
  while (tokens[num++]) ;
  num--;
  _bg = _execute = 0;
  
  if (!strcmp (tokens[0], "jobs")) { 
    list_entries();
    return 0;
  }
  if (num > 1 && !strcmp (tokens[num-1], "&")) { 
    _execute = _bg = 1;
    free (tokens[num-1]);
    tokens[num-1] = NULL;
    return 0;
  }
  if (!strcmp (tokens[0], "fg")) {
    int job = 0;
    pid_t pid;
    if (num > 1)  job = atoi (tokens[1]);
    pid = find_id (job);
    if (pid < 0) {
      printf ("No process %d\n", job);
      return 0;
    }
    if (get_status (job) == STOPPED_S) 
      kill (pid, SIGCONT);
    del_process (pid); 
    set_foreground (pid);
    join (pid, 1);
    return 0;
  }
  if (!strcmp (tokens[0], "bg")) {
    int job = 0;
    pid_t pid;
    if (num > 1)  job = atoi (tokens[1]);
    pid = find_id (job);
    if (pid < 0) {
      printf ("No process %d\n", job);
      return 0;
    }
    change_id_status (job, RUNNING_S);
    kill (pid, SIGCONT);
    return 0;
  }
#if 1
  if (!strcmp (tokens[0], "cd")) {
    static char last_dir[MAX_COMMAND_LENGTH] = { 0, };
    char tmp[MAX_COMMAND_LENGTH];
    char *dir;
    if (num == 1)  dir = getenv ("HOME");
    else if (!strcmp (tokens[1], "-")) {
      if (last_dir[0]) {
	strncpy (tmp, last_dir, sizeof (tmp));
	dir = tmp;
      }
      else { 
	printf ("Previus directory not defined\n");
        return 0;
      }
    }
    else dir = tokens[1];
    getcwd (last_dir, sizeof (last_dir));
    if (chdir (dir) < 0) 
      printf ("%s: %s\n", dir, strerror (errno));
    return 0;
  }
  if (!strcmp (tokens[0], "suspend")) {
    raise (SIGSTOP);
    return 0;
  }
  if (!strcmp (tokens[0], "logout")) {
    logout();
    return 0;
  }
  if (!strcmp (tokens[0], "raise")) {
    int sig = 15;
    if(tokens[1]) {
      int sig_2 = atoi (tokens[1]);
      if (sig_2 > 0) sig = sig_2;
    } 
    raise(sig);
    return 0;
  }
#endif 

  _execute = 1;
  return 0;   
}
