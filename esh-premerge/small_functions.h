#ifndef _SMALL_FUNCTIONS_H
#define _SMALL_FUNCTIONS_H

#include "esh_base.h"
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>

#define R	O_RDONLY
#define W	O_WRONLY
#define A	O_APPEND

struct fds {
//  char *fname;
  int where;
  int which;
};

struct cmd {
  char **tokens;
  struct fds fds[20];
  int fd_num;
  pid_t gid;
};

int add_command_token (const char *token, struct cmd **cmd);
int add_command_redirection (const char *fname, struct cmd **cmd, int fd, int mode);
int add_command_pipe (const char *token, struct cmd **cmd);
int execute_commad (struct cmd **cmd, int fg, int join);


void free2 (void *p1, void *p2);
void free3 (void *p1, void *p2, void *p3);
void free4 (void *p1, void *p2, void *p3, void *p4);


#endif

