/*
 *
 * libesh_base: join_process.c
 *
 * $Id: join_process.c,v 1.4 2004-02-22 18:35:05 razeto Exp $
 *
 * Copyright (C) 2001 Alessandro Razeto
 * Author: Alessandro Razeto <eto@linux.it>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * esh shell base library
 *
*/
#include <sys/types.h>
#include <unistd.h>
#define _USE_BSD
#include <sys/types.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/wait.h>

#include "esh_base.h"
#include "variable_mngr.h"

#ifdef _USE_GLOBAL
#include "global.h"
#else
#include <string.h>
#include <errno.h>
#include <stdio.h>
#define FAIL -1 
#define SYS_ERROR(fmt, args...) ({ \
    fprintf(stderr, "!!! %s in %s,%d >> ", __PRETTY_FUNCTION__, __FILE__, __LINE__); \
    fprintf(stderr, fmt, ##args); \
    fprintf(stderr, ": %s (%d)\n", strerror(errno), errno);  \
    FAIL; })
#define MSG(fmt, args...)       fprintf(stderr, fmt, ##args)
#endif
#define DEBUG(...)

int call_wait (pid_t pid, int flags) {
  int status;
  int id;
  DEBUG ("call_wait (%d, %d)\n", pid, flags);
  if ((pid = waitpid (pid, &status, WUNTRACED | flags)) <0) 
    return -1; 
  if (!pid) return 0;
  id = find_process(pid);
  if (WIFSTOPPED(status)) {
    if (pid == getpgid (pid)) {
      set_foreground (getpid ());
      add_process (pid, STOPPED_S);
      id = find_process(pid);
      MSG ("Stopped process %d [%d]\n", pid, id);
    }
    return pid;
  }
  if (id < 0) { 
    char str_status[128];
    set_foreground (getpid ());

    if (WIFSIGNALED(status)) {
      MSG ("Process %d signaled with %d\n", pid, WTERMSIG(status));
      snprintf (str_status, sizeof (str_status), "%d", WTERMSIG(status) + 128);
    } else 
      snprintf (str_status, sizeof (str_status), "%d", WEXITSTATUS(status)); 
    set_var_value ("?", str_status);
  } else {
    del_process (pid);
    if (WIFEXITED(status)) 
      MSG ("Process %d [%d] exited with %d\n", pid, id, WEXITSTATUS(status));
    if (WIFSIGNALED(status)) 
      MSG ("Process %d [%d] signaled with %d\n", pid, id, WTERMSIG(status));
  }
  return pid;
}

pid_t join (pid_t pid, int block) {
  int id;
  
  if (pid && block) block =  0;
  else block = WNOHANG;
  if (!pid) pid = -1;
  do {
    id = call_wait (pid, block);
    if (!id) break;
  //  MSG ("call_wait (%d, %d) = %d\n", pid, block, id);
    if (id < 0 && errno == ECHILD) break;
  } while (!pid);
  return 0;
}

