/*
 *
 * libesh_base: small_functions
 *
 * $Id: small_functions.c,v 1.1 2004-02-22 18:35:05 razeto Exp $
 *
 * Copyright (C) 2001 Alessandro Razeto
 * Author: Alessandro Razeto <eto@linux.it>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * esh shell base library
 *
*/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#define _INTERNAL_C
#include "esh_base.h"
#include "small_functions.h"

#ifdef _USE_GLOBAL
#include "global.h"
#else
#include <string.h>
#include <errno.h>
#include <stdio.h>
#define MSG(fmt, args...)       fprintf(stderr, fmt, ##args)
#define FAIL -1 
#define SYS_ERROR(fmt, args...) ({ \
    fprintf(stderr, "!!! %s in %s,%d >> ", __PRETTY_FUNCTION__, __FILE__, __LINE__); \
    fprintf(stderr, fmt, ##args); \
    fprintf(stderr, ": %s (%d)\n", strerror(errno), errno);  \
    FAIL; })
#define RNT_ERROR(fmt, args...) ({ \
    fprintf(stderr, "!!! %s in %s,%d >> ", __PRETTY_FUNCTION__, __FILE__, __LINE__); \
    fprintf(stderr, fmt "\n", ##args); \
    FAIL; })
#endif

#define DEBUG(...)	


int add_command_token (const char *token, struct cmd **cmd) {
  if (!*cmd) {
    *cmd = (struct cmd *)calloc (sizeof (struct cmd), 1);
    if (!cmd) return SYS_ERROR("calloc");
  }
  return add_token (token, &((*cmd)->tokens));
}

int add_command_pipe (const char *token, struct cmd **cmd) {
  int *count = &((*cmd)->fd_num);
  struct fds *fd = (*cmd)->fds + *count;
  int pipes[2];
  pid_t pid, old_gid;
  old_gid = (*cmd)->gid;
  if (pipe (pipes)) return SYS_ERROR ("pipe");
  fd->which = pipes[1];
  fd->where = 1;
  (*count)++;
  pid = execute_commad (cmd, 1, 0);
  DEBUG ("PID %d GID %d", pid, old_gid); 
  *cmd = 0;
  add_command_token (token, cmd);
  count = &((*cmd)->fd_num);
  fd = (*cmd)->fds + *count;
  fd->which = pipes[0];
  fd->where = 0;
  (*count)++;
  if (!old_gid) (*cmd)->gid = pid;
  else (*cmd)->gid = old_gid;
  //(*cmd)->gid = pid;;
  DEBUG (" ... next process GID %d\n", (*cmd)->gid); 
  return pid;
}



static int wanna_truncate (void) { return 1; }

int add_command_redirection (const char *fname, struct cmd **cmd, int dest_fd, int mode) {
  int tmpfd;
  int *count = &((*cmd)->fd_num);
  struct fds *fd = (*cmd)->fds + *count;

  if (mode == W) {
    if (!access (fname, F_OK)) {
      if (!wanna_truncate ()) { 
	MSG ("File already %s exist\n", fname);
	return FAIL;
      }
    }
    else mode |= O_CREAT;
  }
  if (mode == R)
    if (access (fname, F_OK)) {
      MSG ("%s: %s\n", fname, strerror(errno)); 
      fd->which = -1;
      (*count)++;
      return FAIL;
    }
  tmpfd = open (fname, mode, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
  if (tmpfd < 0) return SYS_ERROR ("open(%s)", fname);
  fd->which = tmpfd;
  fd->where = dest_fd;
  (*count)++;
  return 0;
}

int execute_commad (struct cmd **cmd, int fg, int call_join) {
  pid_t pid, gid = (*cmd)->gid;
  int i;
  int count = (*cmd)->fd_num;
  struct fds *fd = (*cmd)->fds;
  for (i = 0; i < count; i++) {
    if (fd[i].which < 0) goto clean;
    if (dup2 (fd[i].which, fd[i].where) < 0) return SYS_ERROR("dup2");
  }
  pid = spawn_program ((*cmd)->tokens, gid, fg);

  if (fg) {
    if (call_join) { 
      join (pid, 1);
    }
  } else {
    int id = add_process (pid, RUNNING_S);
    fprintf (stderr, "Process '%s' %d [%d]\n", (*cmd)->tokens[0], pid, id);
  }

clean:
  for (i = 0; i < count; i++) 
    close (fd[i].which);
  restore_fds ();
  free_tokens((*cmd)->tokens);
  free (*cmd);
  *cmd = NULL;
  return pid;
}


void free2 (void *p1, void *p2) { free (p1); free (p2); }
void free3 (void *p1, void *p2, void *p3) { free (p1); free (p2); free (p3); }
void free4 (void *p1, void *p2, void *p3, void *p4) { free (p1); free (p2); free (p3); free (p4); }


#if 0

  if (dup2 (tmpfd, destfd) < 0) return SYS_ERROR("dup2");
  close (tmpfd);


  return 0;
}
#endif

