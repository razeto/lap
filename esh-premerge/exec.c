/*
 *
 * libesh_base: exec.c
 *
 * $Id: exec.c,v 1.2 2004-02-22 18:35:05 razeto Exp $
 *
 * Copyright (C) 2001 Alessandro Razeto
 * Author: Alessandro Razeto <eto@linux.it>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * esh shell base library
 *
*/
#include <sys/types.h>
#include <unistd.h>
#include <string.h>

#include "esh_base.h"

#ifdef _USE_GLOBAL
#include "global.h"
#else
#include <string.h>
#include <errno.h>
#include <stdio.h>
#define FAIL -1 
#define SYS_ERROR(fmt, args...) ({ \
    fprintf(stderr, "!!! %s in %s,%d >> ", __PRETTY_FUNCTION__, __FILE__, __LINE__); \
    fprintf(stderr, fmt, ##args); \
    fprintf(stderr, ": %s (%d)\n", strerror(errno), errno);  \
    FAIL; })
#define RNT_ERROR(fmt, args...) ({ \
    fprintf(stderr, "!!! %s in %s,%d >> ", __PRETTY_FUNCTION__, __FILE__, __LINE__); \
    fprintf(stderr, fmt "\n", ##args); \
    FAIL; })
#define MSG(fmt, args...)       fprintf(stderr, fmt, ##args)
#endif



#if 0
still to be debugged 
int exec (char *command) {
  char line[MAX_COMMAND_LENGTH];
  char *index[MAX_COMMAND_LENGTH]; /* may seem too much but it is
				    * simplest choice */ 
  int i;
  if (strlen (command) > MAX_COMMAND_LENGTH)
    return RNT_ERROR("Command line too long");
    
  memcpy (line, command, MAX_COMMAND_LENGTH);

  
  index[0] = strtok (line, " \v\t\n");
  for(i = 1; i < MAX_COMMAND_LENGTH; i++) {
    index[i] = strtok (NULL, " \v\t\n");
    if (!index[i]) break;
  }

  return exec3 (index[0], index + 1);
}



int exec2 (char *binary, char *arguments) {
  char line[MAX_COMMAND_LENGTH];
  char *index[MAX_COMMAND_LENGTH]; /* may seem too much but it is
				    * simplest choice */ 
  int i;
  if (strlen (arguments) + strlen (binary)  > MAX_COMMAND_LENGTH)
    return RNT_ERROR("Command line too long");
    
  memcpy (line, arguments, MAX_COMMAND_LENGTH);

  
  index[0] = strtok (line, " \v\t\n");
  for(i = 1; i < MAX_COMMAND_LENGTH; i++) {
    index[i] = strtok (NULL, " \v\t\n");
    if (!index[i]) break;
  }

  return exec3 (binary, index);
}

#endif


int exec3 (char *binary, char *argv[]) {

  execvp (binary, argv);
  MSG ("%s: %s\n", binary, strerror(errno)); 
  
  return FAIL;
}
