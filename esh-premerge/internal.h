#ifndef _INTERNAL_H
#define _INTERNAL_H
#include <signal.h>

#include "esh_base.h"



void internal_cd (char *where);
void internal_bg (char *id);
void internal_fg (char *id);
void internal_raise (char *signal);


#ifndef _INTERNAL_C
inline void internal_jobs (void) { list_entries(); }

inline void internal_logout (void) { logout (); }

inline void internal_suspend (void) { raise (SIGSTOP); }

#include "variable_mngr.h"
inline void internal_env (const char *name) { list_var (name); }

inline void internal_export (struct varlist *list) { export_vars (list); }
inline void internal_unset (struct varlist *list) { unset_vars (list); }

#endif



#endif

