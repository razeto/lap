#ifndef _VARIABLE_MNGR_H
#define _VARIABLE_MNGR_H


#define V_ENVIRONMENT	1
#define V_DYNAMIC	2
#define V_CALLBACK	4
#define V_UNLISTABLE	8
#define ENVIROMENT_VAR(a)	((a)->flags == V_ENVIRONMENT)
#define DYNAMIC_VAR(a)		((a)->flags == V_DYNAMIC)
#define CALLBACK_VAR(a)		((a)->flags == V_CALLBACK)
#define UNLISTABLE_VAR(a)	((a)->flags == V_UNLISTABLE)
struct variable {
  struct variable *next;
  char *name;
  int flags;
  char *value;
  char *(*dvalue)();
  char *(*handle)(char *value);
};
#define VARLIST_LIMIT 200
struct varlist {
  int n;
  char *var_names[VARLIST_LIMIT];
};

const char *find_variable (const char *string, int *stringsize);
char * isolate (const char *string, int* size);

int declare_var (const char *name, const char *value, int flags);

//int del_var (const char *name);
int set_var_value (const char *name, const char *value);
const char *get_var_value (const char *name);
void list_var (const char *name);
int set_var_value (const char *name, const char *value);
int add_var (const char *name, const char *value, int flags);

struct varlist * add_var_list (const char *var_name, struct varlist *list);
void var_list_free (struct varlist *list);

void export_vars (struct varlist *list);
void unset_vars (struct varlist *list);
int commit_vars (struct varlist *list);
void backup_vars (struct varlist *list);
int restore_vars (struct varlist *list);

void variable_mngr_init (char **env);
void env_var_create (char **env);
int init_special_variables (void);
#endif
