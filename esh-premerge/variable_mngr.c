/*
 *
 * libesh_base: variable_mngr.c
 *
 * $Id: variable_mngr.c,v 1.1 2004-02-22 18:35:05 razeto Exp $
 *
 * Copyright (C) 2001 Alessandro Razeto
 * Author: Alessandro Razeto <eto@linux.it>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * esh shell base library
 *
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <sys/types.h>
#include <unistd.h>


#include "variable_mngr.h"

#ifdef _USE_GLOBAL
#include "global.h"
#else
#include <string.h>
#include <errno.h>
#include <stdio.h>
#define FAIL -1 
#define SYS_ERROR(fmt, args...) ({ \
    fprintf(stderr, "!!! %s in %s,%d >> ", __PRETTY_FUNCTION__, __FILE__, __LINE__); \
    fprintf(stderr, fmt, ##args); \
    fprintf(stderr, ": %s (%d)\n", strerror(errno), errno);  \
    FAIL; })
#define RNT_ERROR(fmt, args...) ({ \
    fprintf(stderr, "!!! %s in %s,%d >> ", __PRETTY_FUNCTION__, __FILE__, __LINE__); \
    fprintf(stderr, fmt "\n", ##args); \
    FAIL; })
#define MSG(fmt, args...)       fprintf(stderr, fmt, ##args)
#endif



/******************************************************
 * Scanner utility functions to replace $var with its
 * value
 ******************************************************/
inline int isspecial (char c);
const char *find_variable (const char *string, int *stringsize) { 
  char *vname = isolate (string, stringsize);
  const char *value;
  const char *nil = "";

  if (!vname) return nil; 
  value = get_var_value(vname);

  free (vname);
  return value; 
}


char * isolate (const char *string, int* size) {
  extern char *strndup(const char *s, size_t size);
  const char *vname_ptr = string + 1;
  char *copy;
  int len = 1;
  
  if (*vname_ptr == '{') {
    char * tmp = index (vname_ptr,'}');
    if (!tmp) {
      MSG ("Unterminated {\n");
      *size = strlen (string);
      return NULL;
    }
    vname_ptr ++;
    len = tmp - vname_ptr;
  }
  else if (isspecial (*vname_ptr)) {
    len = 2;
  }
  else { 
    register int value = *vname_ptr;
    while (value) {
      if (isalnum (value) || value == '_') value = vname_ptr[len++];
      else break;
    }
  }
  copy = strndup (vname_ptr, len);
  *size = len;
  if (vname_ptr != string + 1) *size += 3;
  return copy;
}

/******************************************************
 * Raw variable element handling and list managing
 ******************************************************/
static struct variable *varlist = NULL;
static struct variable *backup = NULL;
static struct variable *cache = NULL;

static void free_list (struct variable **list) {
  struct variable* var;
  struct variable* next;

  for (var = *list; var; var = next) {
    free (var->name);
    if (var->value) free (var->value);
    next = var->next;
    free (var);
  }
  *list = NULL;
}
int declare_var (const char *name, const char *value, int flags) {
  struct variable* var;
  
  var = (struct variable*)malloc (sizeof (struct variable));
  if (!var) return SYS_ERROR ("malloc");
  var->name = strdup (name);
  if (!var->name) return SYS_ERROR ("strdup");
  var->flags = flags;
  var->value = strdup (value);
  if (!var->value) return SYS_ERROR ("strdup");
  
  var->next = cache;
  cache = var;
  return 0;
}


static int commit_cache (void) {
  struct variable* var;

  for (var = cache; var; var = var->next) 
    if (add_var (var->name, var->value, var->flags)) return FAIL;

  free_list (&cache);
  return 0;
}

static int backup_var (struct variable *orig) {
  struct variable *var;
  if (!orig) return 0;
  
  var = (struct variable*)malloc (sizeof (struct variable));
  if (!var) return SYS_ERROR ("malloc");
  var->name = strdup (orig->name);
  if (!var->name) return SYS_ERROR ("strdup");
  var->flags = orig->flags;
  var->value = strdup (orig->value);
  if (!var->value) return SYS_ERROR ("strdup");
  
  var->next = backup;
  backup = var;
  return 0;
}

static int restore_all_vars (void) {
  struct variable* var;
  int ret = 0;
  for (var = backup; var; var = var->next) 
    if ((ret = add_var (var->name, var->value, var->flags))) break;

  free_list (&backup);
  return ret;
}
  
struct variable* find_var (const char *name, struct variable **prev) {
  struct variable *var;
  if (prev) *prev = NULL;
  if (!varlist) {
    static int check = 0;
    if (check++) MSG ("Searching in empty list\n");
    return NULL;
  }
  for (var = varlist; var; var = var->next) {
    if (!strcmp (name, var->name)) break;
    if (prev) *prev = var;
  }
  return var;
}

int add_var (const char *name, const char *value, int flags) {
  struct variable* var = find_var (name, NULL);
  if (!var) {
    var = (struct variable*)malloc (sizeof (struct variable));
    if (!var) return SYS_ERROR ("malloc");
    var->name = strdup (name);
    if (!var->name) return SYS_ERROR ("strdup");
    var->next = varlist;
    varlist = var;
    var->flags = 0;
    var->value = NULL;
  } if (var->value) { free (var->value); var->value = NULL; }

  var->flags |= flags;
  return set_var_value (name, value);
}

int set_var_value (const char *name, const char *value) {
  struct variable* var = find_var (name, NULL);
  if (!var) return 0;
  
  if (var->value) free (var->value);
  if (value) {
    var->value = strdup (value);
    if (!var->value) return SYS_ERROR ("strdup");
  } else var->value = NULL;
  if (CALLBACK_VAR(var)) (*(var->dvalue))(value);
  
  if (ENVIROMENT_VAR(var)) 
    if (setenv (name, value, 1)) SYS_ERROR ("setenv");

  return 0;
}

static int add_special_var (const char *name, const char *value, int flags, 
    char *(*dvalue)(), char *(*handle)(char *value)) {
  struct variable* var;
  if (add_var (name, value, flags)) return FAIL;
  var = find_var (name, NULL);
  if (!var) return RNT_ERROR ("an just declared var (%s) disappeared", name);
  var->dvalue = dvalue;
  var->handle = handle;
  return 0;
}

  
static int del_var (const char *name) {
  struct variable *var;
  struct variable *prev;

  var = find_var (name, &prev);
  if (!var) {
    /* MSG ("Element %s not found\n", name); */
    return 0;
  }
  if (prev) prev = var->next;
  else varlist = var->next;
  free (var->name);
  free (var->value);
  if (ENVIROMENT_VAR(var)) unsetenv (name);
  free (var);
  return 0;
}

const char *get_var_value (const char *name) {
  struct variable *var;
  const char *nil = "";

  var = find_var (name, NULL);
  if (!var) return nil;
  if (DYNAMIC_VAR(var)) return (*(var->dvalue))();
  else return var->value;
}

void list_var (const char *name) {
#define FORMAT	"%-15s%60s\n"
  char buf[255];
  const char *value;
  if (name) {
    value = get_var_value (name);
    snprintf (buf, 254, "%s:", name);
    printf (FORMAT,buf,value);
  } else {
    struct variable *var;
    if (!varlist) return;
    for (var = varlist; var; var = var->next) {
      if (UNLISTABLE_VAR(var)) continue; 
      snprintf (buf,254,"%s:", var->name);
      if (DYNAMIC_VAR(var)) printf (FORMAT, buf, (*(var->dvalue))());
      else printf (FORMAT, buf, var->value);
    }
  }
#undef FORMAT
}

/******************************************************
 * Var list managing
 ******************************************************/
struct varlist * add_var_list (const char *var_name, struct varlist *list) {
  if (!var_name) return list;
  if (!list) {
    list = (struct varlist *)malloc (sizeof (struct varlist));
    if (!list) {
      SYS_ERROR ("malloc");
      return list;
    }
    list->n = 0;
  }
  if (list->n > VARLIST_LIMIT) {
    MSG ("Too many variables on list\n");
    return list;
  }
  
  list->var_names[list->n] = strdup (var_name);
  if (!list->var_names[list->n]) {
    SYS_ERROR ("strdup");
    return list;
  }
  list->n ++;
  return list;
}
  
void var_list_free (struct varlist *list) {
  if (!list) return;

  for (; list->n > 0; list->n--) 
    free (list->var_names[list->n - 1]);
  free (list);
}


void export_vars (struct varlist *list) {  
  char *value;
  char *name;
  if (!list) return;

   
  for (; list->n > 0; list->n--) {
    struct variable *var;
    name = list->var_names[list->n - 1];
    var = find_var (name, NULL);
    if (!var) {
      add_var (name, NULL, 0);
      list->n++;
      continue;
    }
    
    if (DYNAMIC_VAR(var)) value = (*(var->dvalue))();
    else value =  var->value;
    
    if (!value) continue; /* bash do not export an empty var */
    var->flags |= V_ENVIRONMENT;
    if (setenv (name, value, 1)) SYS_ERROR ("setenv");
    /*MSG ("Exporting %s\n", list->var_names[list->n - 1]);*/
  }
}

void unset_vars (struct varlist *list) {
  if (!list) return;
  
  for (; list->n > 0; list->n--)
    del_var (list->var_names[list->n - 1]);
}

int commit_vars (struct varlist *list) {
  int ret = commit_cache ();
  if (list) var_list_free (list);
  return ret;
}

void backup_vars (struct varlist *list) {
  char *name;
  int n;
  if (!list) return;
  
  n = list->n;
  for (; n > 0; n--) {
    struct variable *var;
    name = list->var_names[list->n - 1];
    var = find_var (name, NULL);
    backup_var (var);
  }
}

int restore_vars (struct varlist *list) {
  int ret = restore_all_vars ();
  if (list) var_list_free (list);
  return ret;
}


/******************************************************
 * Initialization: environment variable table creation
 *   and dynamic/internal variable init
 ******************************************************/
void variable_mngr_init (char **env) {
  env_var_create (env);
  init_special_variables ();
}


static char *environment[];

void env_var_create (char **env) {
  extern char *strndup(const char *s, size_t size);
  char *item = env[0];
  char *lindex;
  int i;
  for (i = 0; item; item = env[++i]) {
    lindex = index (item, '=');
    if (!lindex) add_var (item, NULL, V_ENVIRONMENT);
    else {
      char *name;
      char *value;
      if (lindex == item) continue; /* ignore items done like "=foo" */
      name = strndup (item, lindex - item);
      value = lindex + 1;
      if (!value[0]) value = NULL;
      add_var (name, value, V_ENVIRONMENT);
      free (name);
    }
  }
}

/******************************************************
 * Special variables
 ******************************************************/

static unsigned char special_reg[256] = {};

inline int isspecial (char c) {
 /* test if char != unsigned char */
#ifdef __CHAR_UNSIGNED__
  return special_reg[c];
#else
  return special_reg[c + 128];
#endif
}

struct special_var_ {
  char *name;
  char *(*dvalue)();
  char *(*handle)(char *value);
  int flags;
};


static char answer[4096];

static char *var_getpid (void) {
  snprintf (answer, sizeof (answer), "%d", getpid ());  
  return answer;
}

static char *var_getppid (void) {
  snprintf (answer, sizeof (answer), "%d", getppid ());  
  return answer;
}

static char *var_gethostname (void) {
  gethostname (answer, sizeof (answer));
  return answer;
}

static char *var_getrandom (void) {
  int num = rand ();
  num *= 32767. / RAND_MAX;
  snprintf (answer, sizeof (answer), "%d", num);  
  return answer;
}
  


static struct special_var_ special_var[] = { 
  { "$", var_getpid, NULL, V_DYNAMIC },
  { "PPID", var_getppid, NULL, V_DYNAMIC },
  { "HOSTNAME", var_gethostname, NULL, V_DYNAMIC },
  { "RANDOM", var_getrandom, NULL, V_DYNAMIC },
  { "?", NULL, NULL, 0 },
};
  

int init_special_variables (void) {
  int i;
  for (i = 0; i < (sizeof (special_var) / sizeof (struct special_var_)); i++) {
    add_special_var (special_var[i].name, NULL, special_var[i].flags, 
	special_var[i].dvalue, special_var[i].handle); 
    if (special_var[i].name[1] == 0) {
#ifdef __CHAR_UNSIGNED__
      special_reg[special_var[i].name[0]] = 1;
#else
      special_reg[special_var[i].name[0] + 128] = 1;
#endif
    }
  }
  return 1;
}
