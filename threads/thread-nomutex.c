#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

#define BUF_SIZE 75

struct fill_struct {
	char *buf;
	int size;
	char filling;
	};

/* the buffer */
char buffer[BUF_SIZE+1] = {0, };

void* fill (void* ptr)
{
	struct fill_struct *data = ptr;
	int i;

	while (1) {
		for (i=0; i < data->size; i++) {
			data->buf[i] = data->filling;
//			data->buf[i+1] = 0;
			usleep(5);
		}
	}
}

int main (int argc, char *argv[])
{
	pthread_t thread1;
	pthread_t thread2;
	struct fill_struct thread1_args;
	struct fill_struct thread2_args;

	thread1_args.buf = buffer;
	thread1_args.size = sizeof(buffer);
	thread1_args.filling = 'O';
	pthread_create (&thread1, NULL, &fill, &thread1_args);

	thread2_args.buf = buffer;
	thread2_args.size = sizeof(buffer);
	thread2_args.filling = '.';
	pthread_create (&thread2, NULL, &fill, &thread2_args);

	while (1) {
		puts(buffer);
		usleep(random() % 12345);
	}

	return 0;
}

