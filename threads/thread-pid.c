/***********************************************************************
* Code listing from "Advanced Linux Programming," by CodeSourcery LLC  *
* Copyright (C) 2001 by New Riders Publishing                          *
* See COPYRIGHT for license information.                               *
***********************************************************************/

#include <pthread.h>
#include <stdio.h>
#include <unistd.h>

void* thread_function (void* arg)
{
  int i;

  fprintf (stderr, "child thread pid is %d\n", (int) getpid ());
  fprintf (stderr, "a variable on the stack is located at %p\n", &i);
  /* Spin forever.  */
  while (1);
  return NULL;
}

int main ()
{
  pthread_t thread;
  int j;

  fprintf (stderr, "main thread pid is %d\n", (int) getpid ());
  fprintf (stderr, "a variable on the stack is located at %p\n", &j);
  pthread_create (&thread, NULL, &thread_function, NULL);
  /* Spin forever.  */
  while (1);
  return 0;
}
