#include <stdio.h>
#include <unistd.h>
#include <pthread.h>

int static_var = 42;

void *print_var (void* var)
{
	while (1) {
		printf("   static variable = %d\n", static_var);
		printf("   variable in main() = %d\n", *((int *) var) );
		sleep(1);
	};
}

int main (int argc, char *argv[])
{
  pthread_t my_thread;
  int my_var = 43;

  /* create a thread running print_var() */
  pthread_create (&my_thread, NULL, &print_var, &my_var);

  sleep(10);
  static_var = 200;
  printf("====> Changed static variable <====\n");
  sleep(10);
  my_var = 300;
  printf("====> Changed variable in main() <====\n");
  sleep(10);

  return 0;
}
