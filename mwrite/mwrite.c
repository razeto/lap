/* 
 * mwrite.c: write and read between mmaped memory between related processes
 * 
 * $Id: mwrite.c,v 1.4 2002-04-02 06:47:28 ciro Exp $
 * 
 * Copyright (C) 2001 Alessandro Razeto
 * Author: Alessandro Razeto <eto@linux.it>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Working exercise
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/wait.h>

int main (int argc, char *argv[]) {
  int fd;
  int *ptr;
  int count = 0;
  pid_t pid;
  
  /* !!! in kernel < 2.4 it is not possible to mmap /dev/zero with MAP_SHARED */
  fd = open ("/dev/zero", O_RDWR);
  if (fd < 0) {
    perror ("open /dev/zero");
    exit (1);
  }

  ptr =  (int*)mmap (NULL, 100 * sizeof(int), PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
  if (ptr == MAP_FAILED) {
    perror ("mmap");
    exit (1);
  }

  for (count = 0; count < 100; count++)
    ptr[count] = ((count * 5) % 37) & 0xff7e;
  
  pid = fork ();
  if (pid < 0) {
    perror ("fork");
    exit (1);
  }
  
  if (!pid) {
    int i;
    struct board {
      int base;
      int lenght;
      int irq;
      int reg[20];
    } *board = (struct board*)ptr;
    
    printf ("base %#x lenght %#x irq %#x\n", board->base, board->lenght, board->irq&0xff);
    for (i = 0; i < 10; i++)
      printf ("Reg[%i] = %#x\n", i, board->reg[i]);

    exit (0);
  }

  wait (NULL);
  return 0;

}
