/* 
 * mwrite.c: write and read between mmaped memory between related processes
 * 
 * $Id: mwrite_sample.c,v 1.1 2002-03-02 18:56:28 razeto Exp $
 * 
 * Copyright (C) 2001 Alessandro Razeto
 * Author: Alessandro Razeto <eto@linux.it>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  sample file
 */

#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/wait.h>

int main (int argc, char *argv[]) {
  int fd;
  int *ptr;
  int count = 0;
  pid_t pid;
  
  /* open /dev/zero */
  fd = open (...)
  if (fd < 0) {
    perror ("open /dev/zero");
    exit (1);
  }

  /* mmap at least for int[100] room */

  for (count = 0; count < 100; count++)
    ptr[count] = ((count * 5) % 37) & 0xff7e;
  
  /* lanuch a child */
  pid = ...
  
  if (!pid) {
    int i;
    struct board {
      int base;
      int lenght;
      int irq;
      int reg[20];
    } *board = (struct board*)ptr;
    
    printf ("base %#x lenght %#x irq %#x\n", board->base, board->lenght, board->irq&0xff);
    for (i = 0; i < 10; i++)
      printf ("Reg[%i] = %#x\n", i, board->reg[i]);

    exit (0);
  }

  wait (NULL);
  return 0;

}
