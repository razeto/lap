/* 
 * mwrite2.c: write to a file some lines
 * 
 * $Id: mwrite2.c,v 1.1 2002-03-09 14:25:10 razeto Exp $
 * 
 * Copyright (C) 2001 Alessandro Razeto
 * Author: Alessandro Razeto <eto@linux.it>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Working exercise
 */

#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <libgen.h>

#define MAXSIZE 	10000
int main (int argc, char *argv[]) {
  int fd;
  char *base, *ptr, *end;
  int size;
  
  if (argc != 2) {
    fprintf (stderr, "Usage: %s filename\n", basename(argv[0]));
    exit (1);
  }
  
  fd = open (argv[1], O_RDWR | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
  if (fd < 0) {
    perror ("open");
    exit (1);
  }

  if (lseek (fd, MAXSIZE + 1, SEEK_SET) < 0) {
    perror ("lseek");
    exit (1);
  }
  
  if (write (fd, "", 1) < 0) {
    perror ("write");
    exit(1);
  }
  
  if (lseek (fd, 0, SEEK_SET) < 0) {
    perror ("lseek");
    exit (1);
  }
  
  
  base =  (char *)mmap (NULL, MAXSIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
  if (base == MAP_FAILED) {
    perror ("mmap");
    exit (1);
  }
  
  close (fd);

  ptr = base;
  end = base + MAXSIZE;
  while (ptr < end) {
    size = read (0, ptr, end - ptr);
    if (size < 0) {
      perror ("read");
      exit (1);
    }
    if (!size) break;
    ptr += size;
  }

  
  munmap (base, MAXSIZE);
  truncate (argv[1], ptr - base);
  return 0;

}
