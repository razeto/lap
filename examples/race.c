#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>


void write_single (char *line) {
  char *ptr = line;

  while (*ptr) {
    write (1, ptr, 1);
    ptr++;
  }
}
int main () {
  char buffer [100];
  pid_t pid;

  pid = fork ();

  if (!pid) {
    /* child */
    sleep (1);
    snprintf (buffer, sizeof (buffer), "This is me %d, child of %d\n", getpid (), getppid());
    write_single (buffer);
    exit (0);
  }

  sleep (1);
  snprintf (buffer, sizeof (buffer), "I'm the parent %d of %d\n", getpid (), pid);
  write_single (buffer);

  return 0;
}
