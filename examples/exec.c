#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>


int main (int argc, char **argv) {

  printf ("Going to launch \"sleep 10\"\n");
  sleep (10);

  execl ("/bin/sleep", "sleep", "10", NULL);
  /*execl ("/bin/sleep", "who_will_find_this", "10", NULL);*/

  printf ("Something gone wrong\n");
  perror ("exec");
  
  exit (1);
}
