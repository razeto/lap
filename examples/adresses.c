#include <stdlib.h>
#include <stdio.h>

int inited[30] = {10, };
int uninited;


void function (int *a) {
  *a++;
}
int main (int a, char **b) {
  int stack_variable;
  void *ptr = malloc (10);
  void *ptr2 = malloc (1000*10000);
  printf ("Main                     at %p\n", main);
  printf ("Function                 at %p\n", function);
  printf ("Inited variable          at %p\n", &inited);
  printf ("Uninited variable        at %p\n", &uninited);
  printf ("Allocated small variable at %p\n", ptr);
  printf ("Allocated big variable   at %p\n", ptr2);
  printf ("Stack varable            at %p\n", &stack_variable);
  printf ("Environment variable     at %p\n", getenv ("HOME"));

  return 0;
}

