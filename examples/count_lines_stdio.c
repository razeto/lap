#include <stdio.h>
#include <string.h>

#define BUF_SIZE 6000

int main (int argc, char *argv[]) {
  FILE *file;
  int count = 0;
  char buffer[BUF_SIZE];

  file = fopen (argv[1], "r");

  while (fgets (buffer, sizeof (buffer), file) != NULL)
    if (buffer[strlen(buffer) - 1] == '\n')
      count++;
  
  printf ("Counted %d lines\n", count);

  return 0;

}
