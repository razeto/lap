#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>


#define MTU 1400

int main (int a, char **b) {
  int socket_id;
  struct sockaddr_in server;
  struct hostent *hp;
  struct in_addr addr;
  char *address;	    
  char buffer[2000]; 
  
  if (a != 2) { 
    printf ("Usage: write_udp machine");
    exit (1);
  } else address = b[1];
  
  socket_id = socket (PF_INET, SOCK_DGRAM, 0);
  if (socket_id < 0) { perror ("socket"); exit (1); }

  if(inet_aton(address,&addr)==0) { /* address is not a valid IP */
    hp=gethostbyname(address); /* try name resolution */
    if(hp==0) {
      perror ("unknown host");
      exit (1);
    }
    bcopy((char *)hp->h_addr,(char *)&server.sin_addr,hp->h_length);
  } else server.sin_addr.s_addr = addr.s_addr;
  server.sin_port = htons(5798);
  server.sin_family = AF_INET;

  
  while (1) {
    int n = read (0, buffer, MTU);
    if (n < 0) { perror ("read"); exit (1); }
    if (!n) break;

    n = sendto (socket_id, buffer, n, 0, (struct sockaddr *)&server, sizeof(server));
    if (n < 0) { perror ("sendto"); exit (1); }
  }

  exit (0);

}

