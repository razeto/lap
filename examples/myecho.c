#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]) {
  char buffer[1000];

  if (argc > 1 && argv[1][0] == '-' && argv[1][1] == 'l' && !argv[1][2]) {
    if (setvbuf (stdout, NULL, _IOLBF, 0)) {
      perror ("setvbuf");
      exit (1);
    }
  }

  while (fgets (buffer, sizeof (buffer), stdin))
    fputs (buffer, stdout);

  return 0;

}
  
