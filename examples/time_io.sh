#!/bin/sh

#dd if=/dev/zero of=long_file bs=1k count=10k
./create_long_file.pl 3000000 > long_file

cat > stdio.c << EOF
#include "stdio.h"

int main (int argc, char **argv ) {
  char buffer[100];
  
  while (fgets(buffer, sizeof (buffer), stdin))
    fputs (buffer, stdout);

  return 0;
}
EOF

write_file () {
 cat > low.c << EOF
#include <unistd.h>

int main (int argc, char **argv ) {
  char buffer[$1];
  int count;
  
  while ((count = read (0, buffer, sizeof (buffer))) > 0)
    write (1,  buffer, count);

  return 0;
}
EOF
}



echo "STDLIB"
gcc -o stdio -Wall stdio.c 
time ./stdio < long_file > /dev/null
echo ""

echo "LOW level I/O block size 10"
write_file 10
gcc -o low -Wall low.c 
time ./low < long_file > /dev/null
echo ""

echo "LOW level I/O block size 50"
write_file 50
gcc -o low -Wall low.c 
time ./low < long_file > /dev/null
echo ""

echo "LOW level I/O block size 100"
write_file 100
gcc -o low -Wall low.c 
time ./low < long_file > /dev/null
echo ""

echo "LOW level I/O block size 500"
write_file 500
gcc -o low -Wall low.c 
time ./low < long_file > /dev/null
echo ""

echo "LOW level I/O block size 1000"
write_file 1000
gcc -o low -Wall low.c 
time ./low < long_file > /dev/null
echo ""

#echo "LOW level I/O block size 5000"
#write_file 5000
#gcc -o low -Wall low.c 
#time ./low < long_file > /dev/null
#echo ""

echo "LOW level I/O block size 10000"
write_file 10000
gcc -o low -Wall low.c 
time ./low < long_file > /dev/null
echo ""

rm low.c low stdio stdio.c
