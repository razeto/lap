#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>

int main (int argc, char *argv[]) {

  pid_t pid;

#if 1
  pid = fork ();
#else
  pid = vfork ();
#endif

  if (!pid) {
    printf ("This is me, child %d\n", getpid ());
    sleep (3);
    printf ("This is still the child\n");
    exit (1);
  }

  printf ("I'm the parent %d of %d\n", getpid (), pid);

#if 0
  wait (NULL);

  printf ("Child finished\n");
#endif
  sleep (30);
  printf ("I'm bored, bye\n");
  exit (0);
}
