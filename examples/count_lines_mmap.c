#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>


int main (int argc, char *argv[]) {
  off_t size;
  int fd;
  struct stat stats;
  char *file_ptr, *ptr;
  int count = 0;
  
  fd = open (argv[1], O_RDONLY);
  
  fstat (fd, &stats);

  size = stats.st_size;

  ptr = file_ptr = mmap (NULL, size, PROT_READ, MAP_PRIVATE, fd, 0);
  if (ptr == MAP_FAILED) {
  	perror("mmap");
	exit(1);
  }

  ptr = memchr (file_ptr, '\n', size);
  if (ptr != NULL) {
    count ++;

    while (ptr+1 < (file_ptr + size)) {
      ptr = memchr (ptr+1, '\n', (file_ptr + size) - ptr);
      if (ptr == NULL)
        break;
      count ++;
  	}
  }

  printf ("Counted %d lines\n", count);

  return 0;

}
