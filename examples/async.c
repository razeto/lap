#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>


void handle (int signum) {
  static char buf[1000];
  int count;
  fprintf (stderr, "Data ready\n");
  count = read (0, buf, sizeof (buf));
  if (count < 0) {
    perror ("read");
    exit (1);
  } else if (!count) {
    fprintf (stderr, "Data stream closed\n");
    exit (0);
  }
  buf[count] = 0;
  write (1, buf, count);
}

int main () {
  struct sigaction sa;
  sa.sa_handler = handle;
  sa.sa_flags = 0;

  if (sigaction (SIGIO, &sa, NULL) < 0) {
    perror ("sigaction");
    exit (1);
  }
    
  if (fcntl (0, F_SETFL, O_ASYNC) < 0) {
    perror ("fcntl");
    exit (1);
  }
  if (fcntl (0, F_SETOWN, getpid()) < 0) {
    perror ("fcntl");
    exit (1);
  }

  while (1) pause();

  exit (1);

}



