#ifndef _OPERATION_H
#define _OPERATION_H

int division (int left, int right);
int multiplication (int left, int right);
int subtraction (int left, int right);
int addition (int left, int right);

#endif
