#include <stdio.h>
#include <signal.h>
#include <unistd.h>

void handler(int signo, siginfo_t *info, void *ignored) {
  printf ("Ops! received signal %d from %d\n", signo, info->si_pid);
  exit (0);
}

void handler_old_style (int signo) {
  printf ("received signal %d\n", signo);
}


int main (int argc, char **argv) {
  struct sigaction act;

  act.sa_handler = handler_old_style;

  sigaction (SIGINT, &act, NULL);
  sigaction (SIGHUP, &act, NULL);
  sigaction (SIGTERM, &act, NULL);
  
  act.sa_sigaction = handler;
  act.sa_flags = SA_SIGINFO; 
  sigaction (SIGQUIT, &act, NULL);

  while (1) pause ();

  exit (1);
}
