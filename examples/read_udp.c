#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>




int main (int a, char **b) {
  int socket_id;
  struct sockaddr_in server;
  char buffer[20000]; 
  
  socket_id = socket (PF_INET, SOCK_DGRAM, 0);
  if (socket_id < 0) { perror ("socket"); exit (1); }

  server.sin_addr.s_addr=htonl(INADDR_ANY);
  server.sin_port=htons(5798);
  server.sin_family=AF_INET;
  
  if (bind (socket_id, (struct sockaddr *)&server, sizeof(server)) < 0)
    { perror ("bind"); exit (1); }
  
  while (1) {
    int n = sizeof (buffer);
    socklen_t len = sizeof(struct sockaddr_in);

    n = recvfrom (socket_id, buffer, n, 0, (struct sockaddr *)&server , &len);
    if (n < 0) { perror ("recvfrom"); exit (1); }
    if (!n) break;
    
    n = write (1, buffer, n);
    if (n < 0) { perror ("write"); exit (1); }
  }

  exit (0);

}

