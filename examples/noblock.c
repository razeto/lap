#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <libgen.h>
#include <syslog.h>
#include <errno.h>

int main (int argc, char **argv) {
  int filter = 0;
  char buf[100];
  int ret_code;
  
  if (!isatty (0)) {
    char *name;
    printf ("Stdin is not a tty going in filter mode\n");
    printf ("Further output will be sent to syslog\n");
    filter =1;
    name = basename(argv[0]);
    openlog (name, 0, LOG_LOCAL0);
  }

  if (fcntl (0, F_SETFL, O_NONBLOCK) < 0) {
    perror ("fcntl (O_NONBLOCK)");
    exit (1);
  }

  while (1) {
    ret_code = read (0, buf, sizeof (buf));
    if (!ret_code) break;
    if (ret_code < 0) {
      if (errno == EAGAIN) {
        if (filter) syslog (LOG_NOTICE, "No input\n");
        else printf ("No input\n");
        sleep (3);
      } else { 
        perror ("read");
        exit (1);
      }
    } else {
    /* do filter */
    write (1, buf, ret_code);
    }
  }

  return 0;
}

