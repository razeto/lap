#!/bin/bash


cat > faketime.c << EOF
#include <time.h>
time_t time(time_t *t) {
  *t = 123457;
  return 123457;
}
int clock_gettime(clockid_t clk_id, struct timespec *tp) {
  tp->tv_sec = 1167001200;
  return 0;
}
EOF

gcc -c -fPIC -Wall faketime.c -o faketime.lo
ld -o libfaketime.so faketime.lo -shared -soname=libfaketime.so

date
LD_LIBRARY_PATH=":."
LD_PRELOAD="libfaketime.so"
export LD_PRELOAD LD_LIBRARY_PATH
date


