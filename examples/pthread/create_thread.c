#include <pthread.h>
#include <unistd.h>
#include <stdio.h>

int count = 100;

void* do_thread (void* thread_number) {
  while (count > 0) {
    printf ("thread %d count %d\n", *(int *)(thread_number), count);
    usleep (300000);
    count --;
  }
  return NULL;
}

int main () {
  int i;
  int id[3] = { 1, 2, 3 };
  pthread_t thread_id[3];
  for (i = 0; i < 3; i++) pthread_create (&thread_id[i], NULL, &do_thread, id + i);

  while (count > 0) {
    printf ("thread %d count %d\n", 0, count);
    usleep (1000000);
    count --;
  }

  sleep (20);
  for (i = 0; i < 3; i++) pthread_join (thread_id[i], NULL);

  return 0;
}

