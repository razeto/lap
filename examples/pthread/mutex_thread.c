#include <pthread.h>
#include <unistd.h>
#include <stdio.h>

int count = 10000;
pthread_mutex_t sleep_mutex;
pthread_cond_t start_condition;
pthread_mutex_t start_condition_mutex;

void* do_thread (void* thread_number) {
  printf ("started thread %d\n", *(int *)(thread_number));
  pthread_mutex_lock (&start_condition_mutex);
  pthread_cond_wait (&start_condition, &start_condition_mutex);
  printf ("thread %d woke up\n", *(int *)(thread_number));
  pthread_mutex_unlock (&start_condition_mutex);
  int my_executions = 0;

  while (count > 0) {
    pthread_mutex_lock (&sleep_mutex);
    usleep (3);
    count --;
    pthread_mutex_unlock (&sleep_mutex);
    my_executions++;
  }
  printf ("thread %d did %d executions\n", *(int *)(thread_number), my_executions);
  return NULL;
}

int main () {
  int i;
  int id[3] = { 1, 2, 3 };
  pthread_t thread_id[3];

  pthread_mutex_init (&sleep_mutex, NULL);
  pthread_cond_init (&start_condition, NULL);
  pthread_mutex_init (&start_condition_mutex, NULL);
  for (i = 0; i < 3; i++) pthread_create (&thread_id[i], NULL, &do_thread, id + i);
#if 0
  sleep (1);
  pthread_cond_signal (&start_condition);
  sleep (1);
  pthread_cond_signal (&start_condition);
  sleep (1);
  pthread_cond_signal (&start_condition);
#else
  sleep (1);
  pthread_cond_broadcast (&start_condition);
#endif
  for (i = 0; i < 3; i++) pthread_join (thread_id[i], NULL);

  return 0;
}

